
import { Routes } from "@angular/router";
import { TableAdminComponent } from "../shared/components/table-admin/table-admin.component";
import { ButtonAddComponent } from "../shared/components/button-add/button-add.component";
import { CardMovieComponent } from "../shared/components/card-movie/card-movie.component";
import { CardStartComponent } from "../shared/components/card-start/card-start.component";
import { FormNavBarComponent } from "../shared/components/form-nav-bar/form-nav-bar.component";
import { FormPrincipalComponent } from "../shared/components/form-principal/form-principal.component";
import { NavBarComponent } from "../shared/components/nav-bar/nav-bar.component";
import { NavBarClientComponent } from "../shared/components/nav-bar-client/nav-bar-client.component";
import { SearchInputComponent } from "../shared/components/search-input/search-input.component";
import { SideBarComponent } from "../shared/components/side-bar/side-bar.component";
import { PopUpComponent } from "../shared/components/pop-up/pop-up.component";

export let componentes: Routes=[{
    path:"table", component:TableAdminComponent
},
{
    path:"button", component:ButtonAddComponent
},
{
    path:"cardmovie", component:CardMovieComponent
},
{
    path:"cardstard", component:CardStartComponent
},
{
    path:"formnavbar", component:FormNavBarComponent
},
{
    path:"fomprincipal", component:FormPrincipalComponent
},
{
    path:"navbar", component:NavBarComponent
},
{
    path:"navclient", component:NavBarClientComponent
},
{
    path:"searchInput", component:SearchInputComponent
},
{
    path:"side", component:SideBarComponent
},
{
    path:"pop", component:PopUpComponent
}]