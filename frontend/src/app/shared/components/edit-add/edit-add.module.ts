import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditAddComponent } from './edit-add.component';



@NgModule({
  declarations: [EditAddComponent],
  imports: [
    CommonModule
  ]
})
export class EditAddModule { }
