import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableAdminComponent } from './table-admin.component';
import { RouterLink } from '@angular/router';



@NgModule({
  declarations: [TableAdminComponent],
  imports: [
    CommonModule,
    RouterLink
  ],
  exports:[TableAdminComponent]
})
export class TableAdminModule { }
