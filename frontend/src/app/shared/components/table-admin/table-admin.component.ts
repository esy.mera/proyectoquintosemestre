import { Component, Input} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table-admin',
  templateUrl: './table-admin.component.html',
  styleUrls: ['./table-admin.component.css']
})
export class TableAdminComponent{
  @Input() data: any;
  @Input() ruta: any;

 constructor(private router: Router){}
 onDelete(id: number): void {
  if (confirm('eliminar')) {
    // If the user clicks "OK," navigate to the delete route
    this.router.navigate([this.ruta['delete'], id]);
  }
}
}

