import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonAddComponent } from './button-add.component';



@NgModule({
  declarations: [ButtonAddComponent],
  imports: [
    CommonModule
  ],
  exports:[ButtonAddComponent]
})
export class ButtonAddModule { }
