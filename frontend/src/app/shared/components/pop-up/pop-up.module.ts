import { PopUpComponent } from './pop-up.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [PopUpComponent],
  imports: [
    CommonModule
  ],exports:[PopUpComponent]
})
export class PopUpModule { }
