import { Component, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})

export class PopUpComponent {
  private DOM: any
  constructor(elementRef: ElementRef) { this.DOM = elementRef.nativeElement; }

  alerta() {
    var alerta = this.DOM.querySelector('.alert');

    alerta.classList.add('show');


    setTimeout(function () {
      alerta.classList.remove('show');
    }, 2000);

    console.log(alerta)

  }
}
