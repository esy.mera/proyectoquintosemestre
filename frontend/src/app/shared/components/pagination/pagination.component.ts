import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { paginacion } from 'src/app/core/models/pagination.model';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})

export class PaginationComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router:Router){}
  @Input() pagination: paginacion={
    url: '',
    end: false,
    page: ''
  }
  isDisabled: boolean = false;
  isDisabledNext: boolean = false;
  next=0
  atras=0
 // /admin/admin-start
 ngOnInit(): void {
  this.route.queryParams.subscribe(e=>{
    
    this.pagination.page=e['page']
    this.next=parseInt(this.pagination.page) + 1
    this.atras=parseInt(this.pagination.page) - 1
    if(e['page']<= 1){
      this.isDisabled=true
    }else{
      this.isDisabled=false
    }
    if(this.pagination.end == true){
      this.pagination.end=true
    }else{
      this.pagination.end=false
    }
    
  })

  
}



}
