import { Component, OnInit,} from '@angular/core';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  ngOnInit(): void {
    let userString:any = localStorage.getItem('user');
  if (userString !== null) {
    userString = JSON.parse(userString);
    this.user.name=userString[0]['name']
    this.user.role=userString[0]['idRole']['description']
  } else {
    console.warn('No user data found in local storage.');
  }
  }
  user:any={
    name:'Sebastian Mera',
    role:'administrador'
  }
  links=[
    {link:'/admin/admin-start', name:'Inicio', icono:'bi-house'},
    {link:'/admin/users', name:'Usuarios', icono:'bi-person-circle'},
    {link:'/admin/clients', name:'Clientes', icono:'bi-people'},
    {link:'/admin/premieres', name:'Estrenos', icono:'bi-shop'},
    {link:'/admin/movies', name:'Peliculas', icono:'bi-ui-checks-grid'},
    {link:'/admin/branch', name:'Sucursales', icono:'bi-ui-checks-grid'},
    {link:'/admin/roles', name:'Roles', icono:'bi-gear'}
  ]
}
