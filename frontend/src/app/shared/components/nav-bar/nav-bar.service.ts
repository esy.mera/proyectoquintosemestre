import { ElementRef, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavBarService {
  
  setupEventListeners(dom:any) {
    const toggler = dom.querySelector('.btn');

    if (toggler) {
      toggler.addEventListener('click', () => {
        this.toggleSidebar();
      });
    }
  }

  private toggleSidebar() {
    const sidebar = document.querySelector('#sidebar');

    if (sidebar) {
      sidebar.classList.toggle('collapsed');
    }
  }
}
