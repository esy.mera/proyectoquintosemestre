import { Component, ElementRef, Input} from '@angular/core';
import { NavBarService } from './nav-bar.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent {

  @Input( ) Navbar:string = 'Inicio';
  
  private dom: any;

  constructor(private elementRef: ElementRef, private navbarService:NavBarService, private router: Router) {
    this.dom = this.elementRef.nativeElement;
  }

  ngAfterViewInit(): void {
    this.navbarService.setupEventListeners(this.dom);
  }

  SignOff(){
    localStorage.removeItem('user')
    this.router.navigate(['/'])
  }
}
