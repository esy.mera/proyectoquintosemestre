import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarClientComponent } from './nav-bar-client.component';
import { RouterLink } from '@angular/router';



@NgModule({
  declarations: [NavBarClientComponent],
  imports: [
    CommonModule,
    RouterLink
  ],
  exports:[NavBarClientComponent]
})
export class NavBarClientModule { }
