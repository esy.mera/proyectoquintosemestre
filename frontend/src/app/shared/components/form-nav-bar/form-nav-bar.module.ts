import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormNavBarComponent } from './form-nav-bar.component';
import { RouterLink } from '@angular/router';



@NgModule({
  declarations: [FormNavBarComponent],
  imports: [
    CommonModule,
    RouterLink
  ],
  exports:[FormNavBarComponent]
})
export class FormNavBarModule { }
