import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardStartComponent } from './card-start.component';

describe('CardStartComponent', () => {
  let component: CardStartComponent;
  let fixture: ComponentFixture<CardStartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CardStartComponent]
    });
    fixture = TestBed.createComponent(CardStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
