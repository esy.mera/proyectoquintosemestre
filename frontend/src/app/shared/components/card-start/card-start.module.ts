import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardStartComponent } from './card-start.component';



@NgModule({
  declarations: [CardStartComponent],
  imports: [
    CommonModule
  ],
  exports:[CardStartComponent]
})
export class CardStartModule { }
