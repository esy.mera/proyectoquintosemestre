import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormPrincipalComponent } from './form-principal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { DirectivesModule } from '../../directives/directives.module';



@NgModule({
  declarations:  [FormPrincipalComponent],
  imports: [
    DirectivesModule,
    CommonModule,
    ReactiveFormsModule,
    RouterLink,
    
  ],
  exports: [FormPrincipalComponent]
  
})
export class FormPrincipalModule { }
