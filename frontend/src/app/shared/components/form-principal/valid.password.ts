import { AbstractControl, ValidatorFn, ValidationErrors} from '@angular/forms';

export class CustomValidators {
  static passwordMatchValidator(controlName: string, matchingControlName: string): ValidatorFn {
    
    return (group: AbstractControl): ValidationErrors | null => {
      const password = group.get(controlName)?.value;
      const confirmPassword = group.get(matchingControlName)?.value;
      return password === confirmPassword ? null : { passwordmatch: true };
    };
  }
}