import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomValidators } from 'src/app/shared/components/form-principal/valid.password';

@Component({
  selector: 'app-form-principal',
  templateUrl: './form-principal.component.html',
  styleUrls: ['./form-principal.component.css']
})
export class FormPrincipalComponent implements OnInit{
@Input() title=''
@Input() field: any;
@Output() data= new EventEmitter<string>()
form!: FormGroup<any>;
names:string[]=[]
pasword= ['password', 'paswordConfirm', 'contraseña', 'confirmar contraseña']
response:any={}
formBuilder: any;
constructor( formBuilder: FormBuilder, private route:Router){
  this.formBuilder=formBuilder
}
ngOnInit(): void {
  this.form = this.formBuilder.group(
    {...this.field},{ validator: CustomValidators.passwordMatchValidator('nuevaContraseña', 'confirmarContraseña') }
  );
  Object.keys(this.form?.value).forEach((clave) => {
    this.names.push(clave)
  })
}

verificarPasword(contraseña: string) {
  return !this.pasword.includes(contraseña);
}

emitData(){
  if(this.form.errors){
    if(this.form.errors['passwordmatch']){
      alert('La contraseña no coincide !vuelva a intentarlo¡')
    }
  }else{
    this.response={
      valid:this.form.status,
      value:this.form.value
    }
    this.data.emit(this.response)
  }
  
  
}



}
