import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorInputDirective } from './error-input.directive';



@NgModule({
  declarations: [ErrorInputDirective,],
  imports: [
    CommonModule
  ],
  exports:[
    ErrorInputDirective,
  ]
})
export class DirectivesModule { }
