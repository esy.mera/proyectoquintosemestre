import { Directive, ElementRef, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[appErrorInput]'
})
export class ErrorInputDirective {
  private _errors: ValidationErrors | null = null;
  private _dirty: boolean = false;
  private _touched: boolean = false;
  private _nativeElement: any;

  @Input() set touched(value: boolean) {
    this._touched = value;
    this.setMessege();
  }

  @Input() set dirty(value: boolean) {
    this._dirty = value;
    this.setMessege();
  }

  @Input() set errors(value: ValidationErrors | null) {
    this._errors = value;
    this.setMessege();
  }

  constructor(elementRef: ElementRef) {
    this._nativeElement = elementRef.nativeElement;
  }

  setMessege() {
    if (this._touched || this._dirty) {

      if (this._errors) {
        if (this._errors['required']) {
          this._nativeElement.innerText = 'El campo es requerido';
        } else if (this._errors['email']) {
          this._nativeElement.innerText =
            'El formato del correo electrónico no es válido';

        } else if (this._errors['minlength']) {
          this._nativeElement.innerText = `El número de caracteres debe ser al menos ${this._errors['minlength']['requiredLength']}. Actualmente tiene ${this._errors['minlength']['actualLength']}`;

        } else if (this._errors['password']) {
          this._nativeElement.innerText =
            'La contraseña debe tener entre 5 y 10 caracteres, incluyendo al menos una mayúscula, una minúscula y un número';
        }

        this._nativeElement.classList.add('mostrar');
      } else {
        this._nativeElement.classList.remove('mostrar');
        this._nativeElement.classList.add('ocultar');
      }
    }
  }
}
