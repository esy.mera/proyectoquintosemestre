import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { PremieresModel } from 'src/app/core/models/premieres.model';

@Component({
  selector: 'app-client-start',
  templateUrl: './client-start.component.html',
  styleUrls: ['./client-start.component.css']
})
export class ClientStartComponent implements OnInit {
  data: PremieresModel[]= [];

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.getMovies();
  }

  getMovies(): void {
    this.apiService.get('1',4).subscribe(
      (response) => {
        this.data = response;
      },
      (error) => {
        console.error('Error al obtener los datos de la API:', error);
      }
    );
  }
}
