import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientStartComponent } from './client-start.component';

const routes: Routes = [{ path: '', component: ClientStartComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientStartRoutingModule { }
