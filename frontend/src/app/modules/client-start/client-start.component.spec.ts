import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientStartComponent } from './client-start.component';

describe('ClientStartComponent', () => {
  let component: ClientStartComponent;
  let fixture: ComponentFixture<ClientStartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ClientStartComponent]
    });
    fixture = TestBed.createComponent(ClientStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
