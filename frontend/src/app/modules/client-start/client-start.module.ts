import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientStartRoutingModule } from './client-start-routing.module';
import { ClientStartComponent } from './client-start.component';
import { CardMovieModule } from 'src/app/shared/components/card-movie/card-movie.module';
import { NavBarClientModule } from 'src/app/shared/components/nav-bar-client/nav-bar-client.module';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { RouterLink } from '@angular/router';


@NgModule({
  declarations: [
    ClientStartComponent
  ],
  imports: [
    CommonModule,
    ClientStartRoutingModule,
    NavBarClientModule,
    CardMovieModule,
    RouterLink
  ],
  providers:[
    ApiService,{provide:'url', useValue:'premiere'}
  ]
})
export class ClientStartModule { }
