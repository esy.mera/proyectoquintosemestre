import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecoveryPasswordRoutingModule } from './recovery-password-routing.module';
import { RecoveryPasswordComponent } from './recovery-password.component';
import { FormPrincipalModule } from 'src/app/shared/components/form-principal/form-principal.module';
import { FormNavBarModule } from 'src/app/shared/components/form-nav-bar/form-nav-bar.module';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';


@NgModule({
  declarations: [
    RecoveryPasswordComponent,
  ],
  imports: [
    CommonModule,
    RecoveryPasswordRoutingModule,
    FormPrincipalModule,
    FormNavBarModule
  ],
  providers:[
    ApiService,{provide:'url', useValue:'employee'}
    
  ]
})
export class RecoveryPasswordModule { }
