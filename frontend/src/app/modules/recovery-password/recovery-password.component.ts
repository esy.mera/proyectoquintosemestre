import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-recovery-password',
  templateUrl: './recovery-password.component.html',
  styleUrls: ['./recovery-password.component.css']
})
export class RecoveryPasswordComponent {
  constructor(private apiService: ApiService, private router:Router){}
  form ={
    email: [null, [Validators.required, Validators.email]],
  };

  data={
    valid:'',
    value:{
      email: "",
      idEmployee: 0,
      nameEmployee: ""
    }
  }

  inputData(data:any){
    this.data=data
    if(this.data.valid=="INVALID"){
      console.log('error de formulario')
    } else if (this.data.valid=="VALID"){
      this.apiService.getSerch(data.value['email']).subscribe((response:any)=>{
        if(response[0]){
          this.data.value.idEmployee=response[0]['id']
          this.data.value.nameEmployee=response[0]['name']
          this.apiService.recoveryPassword(this.data.value).subscribe()
          alert(`Mensaje enviado al correo ${this.data.value.email}`)
          this.router.navigate(['/login'])
        }else{
          alert('¡Lo sentimos! el correo que ingreso no pertenese a ninguno de nuestros usuarios')
        }
      })
    }
    
    
  }

}
