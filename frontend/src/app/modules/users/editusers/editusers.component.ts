import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-editusers',
  templateUrl: './editusers.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class EditusersComponent {
  form! : FormGroup<any>;
  formBuilder: any;
  @Input( ) User:string = 'Usuarios';
  dataSend:any;
  idEdit=''
  dataGet:any

  constructor(formBuilder: FormBuilder, private router: Router, private apiService: ApiService, private route: ActivatedRoute ) { 
    this.formBuilder=formBuilder
  }
    

 ngOnInit(): void {
  this.form = this.formBuilder.group(
    {
    username: [null, [Validators.required]],
    contractDate: [null, [Validators.required]],
    salary: [null, [Validators.required]],
    email: [null, [Validators.required]],
    phone: [null, [Validators.required]],
    address: [null, [Validators.required]],
    civil: [null, [Validators.required]],
    gender: [null, [Validators.required]],
    birthdate: [null, [Validators.required]],
    branch: [null, [Validators.required]],
    role: [null, [Validators.required]],

    }
  )

  this.route.params.subscribe(e=>{
    console.log(e['id'])
    if(e['id']){
      this.loadData(e['id'])
      }
      
  })

    
  
 }

 add(){
  if(this.form.status=='INVALID'){
    alert('datos incorrectos')
  }else if(this.form.status=='VALID'){
    this.route.params.subscribe(e=>{
      console.log(e['id'])
      if(e['id']){
        const formValue=this.form.value
    console.log(this.form.value)
    this.dataSend={
      name: formValue['username'],
      contractDate: formValue['contractDate'],
      salary: formValue['salary'],
      email: formValue['email'],
      phone: `+55 ${formValue['phone']}`,
      address: formValue['address'],
      password: 'admin',
      civil: formValue['civil'],
      birthdate: formValue['birthdate'],
      gender: formValue['gender'],
  
      idBranch: {
        id: parseInt(formValue['branch'])
      },
      idRole: {
        id: parseInt(formValue['role'])
      }
    }
    this.apiService.patch(e['id'],this.dataSend).subscribe(response=>{
      if(response && 'affected' in response){
        alert('editado con exito')
        this.router.navigate(['admin/users'])
      }else{
        alert('error al editar, verifique porfavor')
      }
     
    }, error => {
      // Handle the error if the POST request fails
      alert('error al editar, verifique porfavor')
    });
        }
        
    })
    
      
    
  }
  
 }

 loadData(id:any){
  this.apiService.getOne(id).subscribe(e=>{
    this.dataGet= e
    if(this.dataGet){
      console.log('m',this.dataGet)
      this.form.patchValue({
        username: this.dataGet['name'],
        contractDate: this.dataGet['contractDate'],
        salary: this.dataGet['salary'],
        email: this.dataGet['email'],
        phone: this.dataGet['phone'].split(' ')[1],
        address: this.dataGet['address'],
        civil: this.dataGet['civil'],
        gender: this.dataGet['gender'],
        birthdate: this.dataGet['birthdate'],
        branch: this.dataGet['idBranch']['id'],
        role: this.dataGet['idRole']['id'],
    
        });
    }
  })
 }

 redirigirAAddRoles() {
  this.router.navigate(['admin/users']);
}
}
