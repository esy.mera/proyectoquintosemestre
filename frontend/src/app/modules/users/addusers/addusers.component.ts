import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-addusers',
  templateUrl: './addusers.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class AddusersComponent implements OnInit{
  form! : FormGroup<any>;
  formBuilder: any;
  @Input( ) User:string = 'Usuarios';
  dataSend:any;

  constructor(formBuilder: FormBuilder, private router: Router, private apiService: ApiService ) { 
    this.formBuilder=formBuilder
  }
    

 ngOnInit(): void {
     this.form = this.formBuilder.group(
    {
    username: [null, [Validators.required]],
    contractDate: [null, [Validators.required]],
    salary: [null, [Validators.required]],
    email: [null, [Validators.required]],
    phone: [null, [Validators.required]],
    address: [null, [Validators.required]],
    civil: [null, [Validators.required]],
    gender: [null, [Validators.required]],
    birthdate: [null, [Validators.required]],
    branch: [null, [Validators.required]],
    role: [null, [Validators.required]],

    }
  )
  console.log(this.form.controls)
 }

 add(){
  if(this.form.status=='INVALID'){
    alert('datos incorrectos')
  }else if(this.form.status=='VALID'){
    const formValue=this.form.value
    console.log(this.form.value)
    this.dataSend={
      name: formValue['username'],
      contractDate: formValue['contractDate'],
      salary: formValue['salary'],
      email: formValue['email'],
      phone: `+55 ${formValue['phone']}`,
      address: formValue['address'],
      password: 'admin',
      civil: formValue['civil'],
      birthdate: formValue['birthdate'],
      gender: formValue['gender'],
  
      idBranch: {
        id: parseInt(formValue['branch'])
      },
      idRole: {
        id: parseInt(formValue['role'])
      }
    }
    this.apiService.post(this.dataSend).subscribe(response=>{
      if (response && 'id' in response) {
        alert('usuario creado exitosamente')
    
        this.router.navigate(['/admin/users']);
      } else {
       alert('error al crear, verifique porfavor')
      }
    }, error => {
      // Handle the error if the POST request fails
      console.error('Error in POST request:', error);
    });
      
    
  }
  
 }

 redirigirAAddRoles() {
  this.router.navigate(['admin/users']);
}
}
