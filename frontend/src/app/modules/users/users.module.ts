import {  NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { TableAdminModule } from 'src/app/shared/components/table-admin/table-admin.module';
import { NavBarModule } from 'src/app/shared/components/nav-bar/nav-bar.module';
import { SideBarModule } from 'src/app/shared/components/side-bar/side-bar.module';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { PaginationModule } from 'src/app/shared/components/pagination/pagination.module';
import { ButtonAddModule } from 'src/app/shared/components/button-add/button-add.module';
import { EditusersComponent } from './editusers/editusers.component';
import { AddusersComponent } from './addusers/addusers.component';
import { DeleteComponent } from './delete/delete.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';


@NgModule({
  declarations: [
    UsersComponent,
    EditusersComponent,
    AddusersComponent,
    DeleteComponent
  ],
  imports: [
    DirectivesModule,
    ReactiveFormsModule,
    CommonModule,
    UsersRoutingModule,
    TableAdminModule,
    NavBarModule,
    SideBarModule,
    PaginationModule,
    ButtonAddModule,
    
    
  ],
  providers:[
    ApiService,{provide:'url', useValue:'employee'}
  ]
})
export class UsersModule { }
