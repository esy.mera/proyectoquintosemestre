import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users.component';

import { AddusersComponent } from './addusers/addusers.component';
import { EditusersComponent } from './editusers/editusers.component';
import { DeleteComponent } from './delete/delete.component';

const routes: Routes = [{ path: '', component: UsersComponent },
{ path: 'edit-users/:id', component: EditusersComponent },
{ path: 'add-users', component: AddusersComponent },
{ path: 'delete/:id', component: DeleteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
