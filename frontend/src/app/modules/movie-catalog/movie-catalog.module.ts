import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovieCatalogRoutingModule } from './movie-catalog-routing.module';
import { MovieCatalogComponent } from './movie-catalog.component';
import { NavBarClientComponent } from 'src/app/shared/components/nav-bar-client/nav-bar-client.component';
import { CardMovieComponent } from 'src/app/shared/components/card-movie/card-movie.component';
import { NavBarClientModule } from 'src/app/shared/components/nav-bar-client/nav-bar-client.module';
import { CardMovieModule } from 'src/app/shared/components/card-movie/card-movie.module';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';


@NgModule({
  declarations: [
    MovieCatalogComponent
  ],
  imports: [
    CommonModule,
    MovieCatalogRoutingModule,
    NavBarClientModule,
    CardMovieModule
  ],
  providers:[
    ApiService,{provide:'url', useValue:'premiere'},
    
  ]
})
export class MovieCatalogModule { }
