import { Component, NgModule, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { PremieresModel } from 'src/app/core/models/premieres.model';

@Component({
  selector: 'app-movie-catalog',
  templateUrl: './movie-catalog.component.html',
  styleUrls: ['./movie-catalog.component.css']
})
export class MovieCatalogComponent implements OnInit {
  data: PremieresModel[] = [];

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.getMovies();
  }

  getMovies(): void {
    this.apiService.get('1',50).subscribe(
      (response) => {
        this.data = response;
      },
      (error) => {
        console.error('Error al obtener los datos de la API:', error);
      }
    );
  }
}

