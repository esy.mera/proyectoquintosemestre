import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {
  dataSend={
    id:0,
    password:{
      password:''
    }
  }
  constructor(private apiService: ApiService,private route: ActivatedRoute, private router:Router){}
  ngOnInit(): void {
    this.route.params.subscribe(e=>{
      this.dataSend.id=parseInt(e['id']) 
    })
  }
  form ={
    nuevaContraseña: [null, [Validators.required, Validators.minLength(2)]],
    confirmarContraseña: [null, [Validators.required]]
  }

  inputData(data:any){
    this.dataSend.password.password=data['value']['nuevaContraseña']
    if(data['valid']=='INVALID'){
      console.log('mal formulario')
    }else if(data['valid']=='VALID')
      this.apiService.patch(this.dataSend.id,this.dataSend.password).subscribe(e=>{
        alert('Contraseña cambiada exitosamente')
        this.router.navigate(['/login'])
        
      })
    }
  

  
}
 
