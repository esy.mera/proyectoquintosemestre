import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewPasswordRoutingModule } from './new-password-routing.module';
import { NewPasswordComponent } from './new-password.component';
import { FormPrincipalModule } from 'src/app/shared/components/form-principal/form-principal.module';
import { FormNavBarModule } from 'src/app/shared/components/form-nav-bar/form-nav-bar.module';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';


@NgModule({
  declarations: [
    NewPasswordComponent
  ],
  imports: [
    CommonModule,
    NewPasswordRoutingModule,
    FormPrincipalModule,
    FormNavBarModule
  ],
  providers:[
    ApiService,{provide:'url', useValue:'employee'}
    
  ]
  
})
export class NewPasswordModule { }
