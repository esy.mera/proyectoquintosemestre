import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category.component';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';


@NgModule({
  declarations: [
    CategoryComponent
  ],
  imports: [
    CommonModule,
    CategoryRoutingModule
  ],
  providers:[
    ApiService,{provide:'url', useValue:'category'}
  ]
})
export class CategoryModule { }
