import { Component } from '@angular/core';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent {

  table:any={
    head:[
      {name:'id', nameDataBase:'id'},
      {name:'descripcion', nameDataBase:'description'},
      {name:'edad recomendada', nameDataBase:'recommendedAge'},
      {name:'acciones', actions: true},
      
    ],
    body:[]
  }
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.get();
  }

  async get() {
    try {
      this.table.body = await this.apiService.get('1').toPromise();
      console.log(this.table.body);
    } catch (error) {
      console.log(error);
    }
  }
}
