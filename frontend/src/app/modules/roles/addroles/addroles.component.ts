import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-addroles',
  templateUrl: './addroles.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class AddrolesComponent {
  @Input( ) Roles:string = 'Roles';

  form! : FormGroup<any>;
  formBuilder: any;
  dataSend:any;

  constructor(formBuilder: FormBuilder, private router: Router, private apiService: ApiService ) { 
    this.formBuilder=formBuilder
  }
    

 ngOnInit(): void {
     this.form = this.formBuilder.group(
    {
      Descripcion: [null, [Validators.required]],


    }
  )
  console.log(this.form.controls)
 }

 add(){
  if(this.form.status=='INVALID'){
    alert('datos incorrectos')
  }else if(this.form.status=='VALID'){
    const formValue=this.form.value
    console.log(this.form.value)
    this.dataSend={
      Descripcion: formValue['Descripcion'],

    }
    this.apiService.post(this.dataSend).subscribe(response=>{
      if (response && 'id' in response) {
        alert('usuario creado exitosamente')
    
        this.router.navigate(['/admin/roles']);
      } else {
       alert('error al crear, verifique porfavor')
      }
    }, error => {
      // Handle the error if the POST request fails
      console.error('Error in POST request:', error);
    });
      
    
  }
  
 }

 redirigirAAddRoles() {
  this.router.navigate(['admin/roles']);
}
}

