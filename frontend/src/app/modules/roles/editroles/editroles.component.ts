import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-editroles',
  templateUrl: './editroles.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class EditrolesComponent {
  @Input( ) Roles:string = 'Roles';
  form! : FormGroup<any>;
  formBuilder: any;
  dataSend:any;
  idEdit=''
  dataGet:any

  constructor(formBuilder: FormBuilder, private router: Router, private apiService: ApiService, private route: ActivatedRoute ) { 
    this.formBuilder=formBuilder
  }
    

 ngOnInit(): void {
  this.form = this.formBuilder.group(
    {
      description: [null, [Validators.required]],
    

    }
  )

  this.route.params.subscribe(e=>{
    console.log(e['id'])
    if(e['id']){
      this.loadData(e['id'])
      }
      
  })

    
  
 }

 add(){
  if(this.form.status=='INVALID'){
    alert('datos incorrectos')
  }else if(this.form.status=='VALID'){
    this.route.params.subscribe(e=>{
      console.log(e['id'])
      if(e['id']){
        const formValue=this.form.value
    console.log(this.form.value)
    this.dataSend={
      description: formValue['description'],
      
    }
    this.apiService.patch(e['id'],this.dataSend).subscribe(response=>{
      if(response && 'affected' in response){
        alert('editado con exito')
        this.router.navigate(['admin/roles'])
      }else{
        alert('error al editar, verifique porfavor')
      }
     
    }, error => {
      // Handle the error if the POST request fails
      alert('error al editar, verifique porfavor')
    });
        }
        
    })
    
      
    
  }
  
 }

 loadData(id:any){
  this.apiService.getOne(id).subscribe(e=>{
    this.dataGet= e
    if(this.dataGet){
      console.log('m',this.dataGet)
      this.form.patchValue({
        description: this.dataGet['description'],
    
        });
    }
  })
 }

 redirigirAAddRoles() {
  this.router.navigate(['admin/roles']);
}

}
