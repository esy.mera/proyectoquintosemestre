import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolesComponent } from './roles.component';
import { EditrolesComponent } from './editroles/editroles.component';
import { AddrolesComponent } from './addroles/addroles.component';
import { DeleteComponent } from './delete/delete.component';

const routes: Routes = [{ path: '', component: RolesComponent }, 
{ path: 'edit-roles/:id', component: EditrolesComponent },
{ path: 'add-roles', component: AddrolesComponent},
{ path: 'delete/:id', component: DeleteComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesRoutingModule { }
