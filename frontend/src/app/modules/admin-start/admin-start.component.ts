import { Component, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-start',
  templateUrl: './admin-start.component.html',
  styleUrls: ['./admin-start.component.css']
})
export class AdminStartComponent implements OnInit {
  private DOM: any
  windowElement:any
  accepte:string | null=''
  constructor(elementRef: ElementRef) { this.DOM = elementRef.nativeElement; }

  ngOnInit(): void {
    this.accepte=localStorage.getItem('accepted')
    this.windowElement= new (window as any).bootstrap.Modal('#myModal')
    if(this.accepte==='true'){
      this.windowElement.hide()
    }else{
      this.condition(this.windowElement)
    }
    
  }

  condition(element:any) {
    element.show()

  }

  accept(){
    var valueCkeckout = this.DOM.querySelector('#aceptoCheckbox').checked;
    
    if(valueCkeckout){
      localStorage.setItem('accepted', 'true')
      this.windowElement.hide()
    }else{
      alert('acepte los terminos y condiciones')
    }
  }
}
