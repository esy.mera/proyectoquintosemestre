import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminStartRoutingModule } from './admin-start-routing.module';
import { AdminStartComponent } from './admin-start.component';
import { NavBarComponent } from 'src/app/shared/components/nav-bar/nav-bar.component';
import { SideBarComponent } from 'src/app/shared/components/side-bar/side-bar.component';
import { CardStartComponent } from 'src/app/shared/components/card-start/card-start.component';
import { NavBarModule } from 'src/app/shared/components/nav-bar/nav-bar.module';
import { SideBarModule } from 'src/app/shared/components/side-bar/side-bar.module';
import { CardStartModule } from 'src/app/shared/components/card-start/card-start.module';


@NgModule({
  declarations: [
    AdminStartComponent
  ],
  imports: [
    CommonModule,
    AdminStartRoutingModule,
    NavBarModule,
    SideBarModule,
    CardStartModule
  ]
})
export class AdminStartModule { }
