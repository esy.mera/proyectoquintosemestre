import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-addmovies',
  templateUrl: './addmovies.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class AddmoviesComponent {
  @Input( ) Movie:string = 'Peliculas';

  form! : FormGroup<any>;
  formBuilder: any;

  dataSend:any;

  constructor(formBuilder: FormBuilder, private router: Router, private apiService: ApiService ) { 
    this.formBuilder=formBuilder
  }
    

 ngOnInit(): void {
     this.form = this.formBuilder.group(
    {
    name: [null],
    year: [null],
    director: [null, ],
    casting: [null, ],
    synopsis: [null, ],
    duration: [null,],
    address:[null],
    rating: [null],
    productionStudio: [null],
    country: [null],
    language: [null],
    format: [null],
    id_image: [null],
    id_category: [null],

    }
  )
  console.log(this.form.controls)
 }

 add(){
  const formData:any = new FormData();
  formData.append('file', this.form.value['id_image']);
  console.log('m',formData['value'])
  /*if(this.form.status=='INVALID'){
    alert('datos incorrectos')
  }else if(this.form.status=='VALID'){
    const formValue=this.form.value
    console.log(this.form.value)
    this.dataSend={
      name: formValue['name'],
      year: formValue['year'],
      director: formValue['director'],
      casting: formValue['casting'],
      address: formValue['address'],
      synopsis: formValue['synopsis'],
      duration: formValue['duration'],
      rating: formValue['rating'],
      productionStudio: formValue[' productionStudio'],
      country: formValue['country'],
      language: formValue[' language'],
      format: formValue[' format'],
     
  
      idImage: {
        id: parseInt(formValue['image'])
      },
      idCategory: {
        id: parseInt(formValue['category'])
      }
    }
    this.apiService.post(this.dataSend).subscribe(response=>{
      if (response && 'id' in response) {
        alert('usuario creado exitosamente')
    
        this.router.navigate(['/admin/users']);
      } else {
       alert('error al crear, verifique porfavor')
      }
    }, error => {
      // Handle the error if the POST request fails
      console.error('Error in POST request:', error);
    });
      
    
  }*/
  
 }

 redirigirAAddRoles() {
  this.router.navigate(['admin/movies']);
}
}


