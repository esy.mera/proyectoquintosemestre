import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MoviesComponent } from './movies.component';
import { EditmoviesComponent } from './editmovies/editmovies.component';
import { AddmoviesComponent } from './addmovies/addmovies.component';

const routes: Routes = [{ path: '', component: MoviesComponent },
{ path: 'edit-movies', component: EditmoviesComponent },
{ path: 'add-movies', component: AddmoviesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }
