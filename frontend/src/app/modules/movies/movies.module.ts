import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';
import { SideBarModule } from 'src/app/shared/components/side-bar/side-bar.module';
import { NavBarModule } from 'src/app/shared/components/nav-bar/nav-bar.module';
import { TableAdminModule } from 'src/app/shared/components/table-admin/table-admin.module';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { PaginationModule } from 'src/app/shared/components/pagination/pagination.module';
import { ButtonAddModule } from 'src/app/shared/components/button-add/button-add.module';
import { AddmoviesComponent } from './addmovies/addmovies.component';
import { EditmoviesComponent } from './editmovies/editmovies.component';
import { DeleteComponent } from './delete/delete.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    MoviesComponent,
    AddmoviesComponent,
    EditmoviesComponent,
    DeleteComponent
  ],
  imports: [
    DirectivesModule,
    ReactiveFormsModule,
    CommonModule,
    MoviesRoutingModule,
    TableAdminModule,
    NavBarModule,
    SideBarModule,
    PaginationModule,
    ButtonAddModule
  ],
  providers:[
    ApiService,{provide:'url', useValue:'movie'}
  ]
})
export class MoviesModule { }
