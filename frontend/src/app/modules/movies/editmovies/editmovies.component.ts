import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-editmovies',
  templateUrl: './editmovies.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class EditmoviesComponent {
  @Input( ) Movie:string = 'Peliculas';

}
