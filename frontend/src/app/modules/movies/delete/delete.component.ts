import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent {
  constructor(private apiService:ApiService, private route:ActivatedRoute, private router:Router){}
  ngOnInit(): void {
      this.route.params.subscribe(responce=>{
        this.apiService.delete(responce['id']).subscribe(responce=>{console.log (responce)})
        this.router.navigate(['/admin/movies'])
      })
  }
}
