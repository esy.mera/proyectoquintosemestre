import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsComponent } from './clients.component';
import { NavBarClientModule } from 'src/app/shared/components/nav-bar-client/nav-bar-client.module';
import { CardMovieModule } from 'src/app/shared/components/card-movie/card-movie.module';
import { SideBarModule } from 'src/app/shared/components/side-bar/side-bar.module';
import { NavBarModule } from 'src/app/shared/components/nav-bar/nav-bar.module';
import { TableAdminModule } from 'src/app/shared/components/table-admin/table-admin.module';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { PaginationModule } from 'src/app/shared/components/pagination/pagination.module';
import { ButtonAddModule } from 'src/app/shared/components/button-add/button-add.module';
import { EditclientsComponent } from './editclients/editclients.component';
import { AddclientsComponent } from './addclients/addclients.component';
import { DeleteComponent } from './delete/delete.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ClientsComponent,
    EditclientsComponent,
    AddclientsComponent,
    DeleteComponent
  ],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    TableAdminModule,
    NavBarModule,
    SideBarModule,
    PaginationModule,
    ButtonAddModule,
    DirectivesModule,
    ReactiveFormsModule

  ],
  providers:[
    ApiService,{provide:'url', useValue:'client'}
  ],
})
export class ClientsModule { }
