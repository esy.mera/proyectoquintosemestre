import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-addclients',
  templateUrl: './addclients.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class AddclientsComponent implements OnInit{
  form! : FormGroup<any>;
  formBuilder: any;
  @Input( ) Client:string = 'Cliente';
  dataSend:any;
    
    constructor(formBuilder: FormBuilder, private router: Router, private apiService: ApiService ) { 
      this.formBuilder=formBuilder
    }
      
  
   ngOnInit(): void {
       this.form = this.formBuilder.group(
      {
      identificationCard: [null, [Validators.required]],
      names: [null, [Validators.required]],
      email: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      gender: [null, [Validators.required]],
      age: [null, [Validators.required]],
      }
    )
    console.log(this.form.controls)
   }
  
   add(){
    if(this.form.status=='INVALID'){
      alert('datos incorrectos')
    }else if(this.form.status=='VALID'){
      const formValue=this.form.value
      console.log(this.form.value)
      this.dataSend={
        identificationCard: formValue['identificationCard'],
        names: formValue['names'],
        email: formValue['email'],
        phone: `+55 ${formValue['phone']}`,
        gender: formValue['gender'],
        age: formValue['age'], 
      }
      this.apiService.post(this.dataSend).subscribe(response=>{
        if (response && 'id' in response) {
          alert('usuario creado exitosamente')
      
          this.router.navigate(['/admin/clients']);
        } else {
         alert('error al crear, verifique porfavor')
        }
      }, error => {
        // Handle the error if the POST request fails
        console.error('Error in POST request:', error);
      });
        
      
    }
    
   }
  
   redirigirAAddRoles() {
    this.router.navigate(['admin/clients']);
  }
}
