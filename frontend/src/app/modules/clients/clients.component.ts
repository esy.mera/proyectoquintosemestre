import { Component, Input} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { paginacion } from 'src/app/core/models/pagination.model';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['../../shared/admin-css/admin-css.css']
})
export class ClientsComponent {
  @Input( ) Clientes:string = 'Clientes';
  table: any = {
    head: [
      { name: 'cedula', nameDataBase: 'identificationCard' },
      { name: 'mombre', nameDataBase: 'names' },
      { name: 'email', nameDataBase: 'email' },
      { name: 'telefonp', nameDataBase: 'phone' },
      { name: 'genero', nameDataBase: 'gender' },
      { name: 'edad', nameDataBase: 'age' },
      { name: 'acciones', actions: true },
    ],
    body: []
  };

  
  nextPage:any={
    body:[]
  }
  paginacion: paginacion = {
    page: '0',
    url: '/admin/clients',
    end: false
  }
  acionesruta= {
    edit: `${this.paginacion.url}/edit-clients/`,
    delete: `${this.paginacion.url}/delete/`
  }
  constructor(private apiService: ApiService, private route: ActivatedRoute, private router: Router) { }
  redirigirAAddRoles() {
    this.router.navigate(['/admin/clients/add-clients']);
  }
  ngOnInit() {
    this.route.queryParams.subscribe(e => {
      if (!e['page'] || parseInt(e['page']) <= 0) {
        this.router.navigate([this.paginacion.url], {
          queryParams: {
            page: 1
          }
        })
      }
      if(e['page']){
        const nextResponse= parseInt(e['page']) + 1
        this.next(JSON.stringify(nextResponse))
        
        this.paginacion.page = e['page']
        this.get();
      }else{
        this.paginacion.page = '1'
        this.next('2')
      }
      
    })
  }

  async get() {
    try {
      this.table.body = await this.apiService.get(this.paginacion.page).toPromise();
      console.log(this.table.body);
    } catch (error) {
      console.log(error);
    }
  }

  async next(page:string) {
    //this.paginacion['page'] - 1
    try {
      this.nextPage.body = await this.apiService.get(page).toPromise();
      if(this.nextPage.body.length <= 0){
        this.paginacion.end=true
      }else{
        this.paginacion.end=false
      }

    } catch (error) {
      console.log(error);
    }
  }
}