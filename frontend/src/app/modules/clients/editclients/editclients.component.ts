import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-editclients',
  templateUrl: './editclients.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class EditclientsComponent {
  @Input( ) Client:string = 'Cliente';

}
