import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientsComponent } from './clients.component';
import { EditclientsComponent } from './editclients/editclients.component';
import { AddclientsComponent } from './addclients/addclients.component';

const routes: Routes = [{ path: '', component: ClientsComponent },
{ path: 'edit-clients', component: EditclientsComponent },
{ path: 'add-clients', component: AddclientsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
