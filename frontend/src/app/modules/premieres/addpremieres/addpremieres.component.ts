import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-addpremieres',
  templateUrl: './addpremieres.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class AddpremieresComponent {
  form! : FormGroup<any>;
  formBuilder: any;
  @Input( ) Premiere:string = 'Estrenos';

  dataSend:any;

  constructor(formBuilder: FormBuilder, private router: Router, private apiService: ApiService ) { 
    this.formBuilder=formBuilder
  }
    

 ngOnInit(): void {
     this.form = this.formBuilder.group(
    {
    premireData: [null, [Validators.required]],
    hour: [null, [Validators.required]],
    cinema: [null, [Validators.required]],
    price: [null, [Validators.required]],
    totalTickets: [null, [Validators.required]],
    reservation: [null, [Validators.required]],
    idMovie: [null, [Validators.required]],
    idBranch: [null, [Validators.required]],
    }
  )
  console.log(this.form.controls)
 }

 add(){
  if(this.form.status=='INVALID'){
    alert('datos incorrectos')
  }else if(this.form.status=='VALID'){
    const formValue=this.form.value
    console.log(this.form.value)
    this.dataSend={
      premireData: formValue['premireData'],
      hour: formValue['hour'],
      cinema: formValue['cinema'],
      price: formValue['price'],
      totalTickets: formValue['totalTickets'],
      reservation: formValue['reservation'],
  
      idMovie: {
        id: parseInt(formValue['movie'])
      },
      idBranch: {
        id: parseInt(formValue['branch'])
      }
    }
    this.apiService.post(this.dataSend).subscribe(response=>{
      if (response && 'id' in response) {
        alert('usuario creado exitosamente')
    
        this.router.navigate(['/admin/premieres']);
      } else {
       alert('error al crear, verifique porfavor')
      }
    }, error => {
      // Handle the error if the POST request fails
      console.error('Error in POST request:', error);
    });
      
    
  }
  
 }

 redirigirAAddRoles() {
  this.router.navigate(['admin/premieres']);
}

}
