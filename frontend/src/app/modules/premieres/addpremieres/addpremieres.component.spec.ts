import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddpremieresComponent } from './addpremieres.component';

describe('AddpremieresComponent', () => {
  let component: AddpremieresComponent;
  let fixture: ComponentFixture<AddpremieresComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AddpremieresComponent]
    });
    fixture = TestBed.createComponent(AddpremieresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
