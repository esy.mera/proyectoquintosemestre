import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-editpremieres',
  templateUrl: './editpremieres.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class EditpremieresComponent {
  @Input( ) Premiere:string = 'Estrenos';

}
