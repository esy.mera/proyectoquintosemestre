import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditpremieresComponent } from './editpremieres.component';

describe('EditpremieresComponent', () => {
  let component: EditpremieresComponent;
  let fixture: ComponentFixture<EditpremieresComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditpremieresComponent]
    });
    fixture = TestBed.createComponent(EditpremieresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
