import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PremieresComponent } from './premieres.component';
import { EditpremieresComponent } from './editpremieres/editpremieres.component';
import { AddpremieresComponent } from './addpremieres/addpremieres.component';

const routes: Routes = [{ path: '', component: PremieresComponent },
{ path: 'edit-premieres', component: EditpremieresComponent },
{ path: 'add-premieres', component: AddpremieresComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PremieresRoutingModule { }
