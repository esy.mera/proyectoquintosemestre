import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { LoginModel } from 'src/app/core/models/login.model';

interface value{
  password:string,
  username:string
}

interface data{
  valid: string;
  value: value;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private apiService: ApiService, private router: Router){}
  ngOnInit(): void {
    if(localStorage.getItem('user')){
      this.router.navigate(['admin/admin-start'])
    }
  }
  form ={
    username: [null, [Validators.required, Validators.minLength(5), Validators.email]],
    password: [null, [Validators.required]],

  };

  dataSend:LoginModel={
    password: '',
    user: ''
  }
  data:data={
    valid:'',
    value:{
      password: '',
      username: ''
    }
  }
  private user:any

  inputData(data:any){
    this.data=data
    if(this.data.valid=="INVALID"){
      console.log('error de formulario')
    } else if (this.data.valid=="VALID"){
      this.dataSend.password=this.data.value['password']
      this.dataSend.user=this.data.value['username']
      this.apiService.login(this.dataSend).subscribe(e => {
        this.user = e;
        if(this.user.length===0){
          alert('usuario incorrecto')
        }else{
          localStorage.setItem('user',JSON.stringify(this.user))
          this.router.navigate(['admin/admin-start'])
        }
        console.log(this.user)
      })
    }
    
    
  }
}
