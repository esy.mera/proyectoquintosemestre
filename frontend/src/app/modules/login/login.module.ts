import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormPrincipalModule } from 'src/app/shared/components/form-principal/form-principal.module';
import { FormNavBarModule } from 'src/app/shared/components/form-nav-bar/form-nav-bar.module';



@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormPrincipalModule,
    FormNavBarModule
  ],
  providers:[ 
    ApiService,{provide:'url', useValue:'login'}
]
})
export class LoginModule { }
