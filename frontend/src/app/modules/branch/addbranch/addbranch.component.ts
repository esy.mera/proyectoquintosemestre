import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';

@Component({
  selector: 'app-addbranch',
  templateUrl: './addbranch.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class AddbranchComponent {
  @Input( ) Branch:string = 'Sucursales';
  form! : FormGroup<any>;
  formBuilder: any;
  dataSend:any;

  constructor(formBuilder: FormBuilder, private router: Router, private apiService: ApiService ) { 
    this.formBuilder=formBuilder
  }
    

 ngOnInit(): void {
     this.form = this.formBuilder.group(
    {
    name: [null, [Validators.required]],
    address: [null, [Validators.required]],
    phone: [null, [Validators.required]],
    peopleCapacity: [null, [Validators.required]],
    schedule: [null, [Validators.required]],
    idCity: [null, [Validators.required]],

    }
  )
  console.log(this.form.controls)
 }

 add(){
  if(this.form.status=='INVALID'){
    alert('datos incorrectos')
  }else if(this.form.status=='VALID'){
    const formValue=this.form.value
    console.log(this.form.value)
    this.dataSend={
      name: formValue['name'],
      address: formValue['ddress'],
      phone: `+55 ${formValue['phone']}`,
      peopleCapacity: formValue['peopleCapacity'],
      schedule: formValue['schedule'],
  
      idCity: {
        id: parseInt(formValue['city'])
      },

    }
    this.apiService.post(this.dataSend).subscribe(response=>{
      if (response && 'id' in response) {
        alert('usuario creado exitosamente')
    
        this.router.navigate(['/admin/branch']);
      } else {
       alert('error al crear, verifique porfavor')
      }
    }, error => {
      // Handle the error if the POST request fails
      console.error('Error in POST request:', error);
    });
      
    
  }
  
 }

 redirigirAAddRoles() {
  this.router.navigate(['admin/branch']);
}

}
