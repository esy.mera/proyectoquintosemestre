import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-editbranch',
  templateUrl: './editbranch.component.html',
  styleUrls: ['../../../shared/admin-css/admin-css.css','../../../shared/admin-css/formularios.css']
})
export class EditbranchComponent {
  @Input( ) Roles:string = 'Sucursales';

}
