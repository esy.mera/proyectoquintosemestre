import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BranchRoutingModule } from './branch-routing.module';
import { BranchComponent } from './branch.component';
import { TableAdminModule } from 'src/app/shared/components/table-admin/table-admin.module';
import { NavBarModule } from 'src/app/shared/components/nav-bar/nav-bar.module';
import { SideBarModule } from 'src/app/shared/components/side-bar/side-bar.module';
import { ApiService } from 'src/app/core/http/api-prefix.interceptor';
import { PaginationModule } from 'src/app/shared/components/pagination/pagination.module';
import { ButtonAddModule } from 'src/app/shared/components/button-add/button-add.module';
import { EditbranchComponent } from './editbranch/editbranch.component';
import { AddbranchComponent } from './addbranch/addbranch.component';
import { DeleteComponent } from './delete/delete.component';
import { DirectivesModule } from 'src/app/shared/directives/directives.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    BranchComponent,
    EditbranchComponent,
    AddbranchComponent,
    DeleteComponent,
  ],
  imports: [
    CommonModule,
    BranchRoutingModule,
    TableAdminModule,
    NavBarModule,
    SideBarModule,
    PaginationModule,
    ButtonAddModule,
    DirectivesModule,
    ReactiveFormsModule
  ],
  providers:[
    ApiService,{provide:'url', useValue:'branch'}
  ]
})
export class BranchModule { }
