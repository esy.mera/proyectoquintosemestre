export interface RolesModel {

    id: number;
    description: string;
    createdAt: Date
    updatedAt: Date
}
