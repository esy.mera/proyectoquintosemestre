export interface UsersModel {
    id: number;
    name: string;
    contractDate: Date;
    salary: string;
    email: string;
    phone: string;
    address: string;
    password: string;
    civil: string;
    birthdate: Date;
    gender: string;
    createdAt: Date
    updatedAt: Date
}
