import { CategoryModel } from "./category.model";
import { MovieModel } from "./movie.model";

export interface PremieresModel {
    id:number;
    premiereDate: Date;
    hour: string;
    cinema: number;
    price: number;
    totalTickets: number;
    reservation: number;
    idMovie: MovieModel;
}
