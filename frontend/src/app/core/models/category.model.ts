export interface CategoryModel {

    id: number;
    description: string;
    recommendedAge: Number;
}
