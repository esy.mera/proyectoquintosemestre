export interface BranchModel {

    id: number;
    name: string;
    address: string;
    phone: Number;
    peopleCapacity: Number;
    schedule: string;
    createdAt: Date
    updatedAt: Date
}
