import { CategoryModel } from "./category.model";

export interface MovieModel {
    id: number;
    name: string;
    year: Date;
    director: string;
    casting: string;
    synopsis: string
    duration: string;
    rating: string;
    productionStudio: string;
    country: string; 
    language: string;
    format: String;
    idCategory: CategoryModel;

}