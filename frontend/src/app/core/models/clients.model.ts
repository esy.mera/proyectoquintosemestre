export interface ClientsModel {
    id: number;
    identificationCard: string;
    names: string;
    email: string;
    phone: string;
    gender: string;
    age: number;
    createdAt: Date;
    updatedAt: Date;
}

