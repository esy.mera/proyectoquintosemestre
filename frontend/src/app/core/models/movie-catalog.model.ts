import { CategoryModel } from "./category.model";
import { MovieModel } from "./movie.model";

export interface MovieCatalogModel {
 idMovie:MovieModel
 idCategory:CategoryModel
}
