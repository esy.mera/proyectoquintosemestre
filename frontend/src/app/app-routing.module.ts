import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './core/guards/admin.guard';

const routes: Routes = [
  { path: '',   redirectTo: '/client/client-start', pathMatch: 'full' },
  { path: 'client', children:[
      { path: 'client-start', loadChildren: () => import('./modules/client-start/client-start.module').then(m => m.ClientStartModule) },
      { path: 'movie-catalog', loadChildren: () => import('./modules/movie-catalog/movie-catalog.module').then(m => m.MovieCatalogModule) },
    ] 
  },
  {
    path: 'admin', canActivate:[AdminGuard] , children:[
      { path: 'admin-start', loadChildren: () => import('./modules/admin-start/admin-start.module').then(m => m.AdminStartModule) },
      { path: 'users', loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule) },
      { path: 'clients', loadChildren: () => import('./modules/clients/clients.module').then(m => m.ClientsModule) },
      { path: 'premieres', loadChildren: () => import('./modules/premieres/premieres.module').then(m => m.PremieresModule) },
      { path: 'roles', loadChildren: () => import('./modules/roles/roles.module').then(m => m.RolesModule) },
      { path: 'movies', loadChildren: () => import('./modules/movies/movies.module').then(m => m.MoviesModule) },
      { path: 'branch', loadChildren: () => import('./modules/branch/branch.module').then(m => m.BranchModule) },
      { path: 'category', loadChildren: () => import('./modules/category/category.module').then(m => m.CategoryModule) },
    ]
  },

  { path: 'login', loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule) },
  { path: 'recovery-password', loadChildren: () => import('./modules/recovery-password/recovery-password.module').then(m => m.RecoveryPasswordModule) },
  { path: 'new-password/:task/:id', loadChildren: () => import('./modules/new-password/new-password.module').then(m => m.NewPasswordModule) },
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
