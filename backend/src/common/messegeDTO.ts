import { ValidationOptions } from "class-validator";


export function minLengthValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property debe ser mayor o igual a $constraint1 caracteres",
    };
}

export function maxLengthValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property debe ser menor o igual a $constraint1 caracteres",
    };
}

export function IsDateValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property debe ser una fecha",
    };
}

export function IsNotEmptyValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property no debe ser null, vacio o undefined",
    };
}

export function IsStringValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property no debe ser de tipo string",
    };
}

export function IsNumberValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property no debe ser de tipo numerico",
    };
}

export function IsDesimalValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property debe ser de tipo decimal",
    };
}

export function IsEmailValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property debe ser in correo electronico",
    };
}

export function IsPhoneNumberValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property debe ser un numero de telefono",
    };
}

export function IsArrayValidationOption(validationOption?: ValidationOptions) {
    return {
        message:
            "El atributo $property debe ser un array",
    };
}