import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { RecoverPasswordService } from './recover-password.service';
import { recoverPasswordDto } from './DTO/recover-password';


@ApiTags('recover-password')
@Controller('recover-password')
export class RecoverPasswordController {

    constructor(private readonly RecoverPasswordService: RecoverPasswordService) {}

    @ApiOperation({ description: "send message to email", summary: "send message to email", })
    @Post("send-message-to-email")
    async sending_message(@Body() payload:recoverPasswordDto) {
        return await this.RecoverPasswordService.sending_message(payload)
    }
    
}
