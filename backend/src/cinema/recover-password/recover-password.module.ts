
import { Module } from '@nestjs/common';
import { RecoverPasswordController } from './recover-password.controller';
import { RecoverPasswordService } from './recover-password.service';
import { transport } from 'src/config/mailer';


@Module({
  imports: [transport],
  controllers: [RecoverPasswordController],
  providers: [RecoverPasswordService]
})
export class RecoverPasswordModule {}
