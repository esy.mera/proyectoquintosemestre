import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { recoverPasswordDto } from './DTO/recover-password';
import { config_env } from 'src/config/config';
import { message_email } from './message-email';

@Injectable()
export class RecoverPasswordService {

    constructor(private readonly mailerService: MailerService){}

    async sending_message(payload:recoverPasswordDto){
        return await this.mailerService.sendMail({
            from: `"TICKETS RECOVER PASSWORD 👻" <${config_env.MAILER.MAIL_USER}>`, // sender address
            to: payload.email, // list of receivers
            subject: "HELLO ✔", // Subject line
            html: message_email(payload.idEmployee, payload.nameEmployee), // html body
        });
    }
}
