import { IsEmail, IsNotEmpty, IsNumber, IsString} from "class-validator";
import { IsNotEmptyValidationOption, IsEmailValidationOption, IsNumberValidationOption, IsStringValidationOption } from "src/common/messegeDTO";


export class recoverPasswordDto {

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsEmail({}, IsEmailValidationOption())
    email: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsNumber({}, IsNumberValidationOption())
    idEmployee: number;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    nameEmployee: string;

}
