import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query } from "@nestjs/common";
import { ApiOperation, ApiTags } from "@nestjs/swagger";
import { ChangeclientPremiereDto } from "./DTO/Change-state-client_premiere.dto";
import { CreateclientPremiereDto } from "./DTO/CreateClient_premiere.dto";
import { UpdateclientPremiereDto } from "./DTO/UpdateClient_premiere.dto";
import { clientPremiereService } from "./client_premiere.service";
import { globalAplicationDTO } from "src/global-dto/global-aplication.dto";

@ApiTags('clientpremiere')
@Controller('clientpremiere')
export class ClientPremiereController {

    constructor(private readonly ClientPremiereService:clientPremiereService){}

    @ApiOperation({ description: "find client premise", summary: "find client premise", })
    @Get()
    async find(@Query() pagination:globalAplicationDTO) {
        return await this.ClientPremiereService.find(pagination)
    }

    @ApiOperation({ description: "find One client premise", summary: "find One client premise", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        return await this.ClientPremiereService.findOne(id);
    }


    @ApiOperation({ description: "Create client premise", summary: "Create client premise", })
    @Post()
    async create(@Body() payload: CreateclientPremiereDto) {
        return await this.ClientPremiereService.create(payload);
    }



    @ApiOperation({ description: "update client premise", summary: "update client premise", })
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdateclientPremiereDto) {
        return await this.ClientPremiereService.Put(id,payload);
    }


    @ApiOperation({ description: "change client premise", summary: "change client premise", })
    @Patch(':id')
    async changeState(@Param('id', ParseIntPipe) id: number, @Body() payload:ChangeclientPremiereDto) {
        return await this.ClientPremiereService.changeState(id,payload);
    }


    @ApiOperation({ description: "delete client premise", summary: "delete client premise", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.ClientPremiereService.delete(id);
    }
}
