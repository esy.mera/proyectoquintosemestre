import { Type } from "class-transformer";
import { IsNotEmpty, IsString, IsDate } from "class-validator";
import { ClientEntity } from "src/cinema/client/client.entity";
import { PremiereEntity } from "src/cinema/premiere/premiere.entity";
import { IsNotEmptyValidationOption, IsStringValidationOption, IsDateValidationOption } from "src/common/messegeDTO";

export class clientPremiereDto{

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    seat: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    idPremiere: PremiereEntity;

    @IsNotEmpty(IsNotEmptyValidationOption())
    idClient: ClientEntity;
}