import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ClientPremiereController } from "./client_premier.controller";
import { clientPremiereEntity } from "./client_premiere.entity";
import { clientPremiereService } from "./client_premiere.service";


@Module({
    imports: [TypeOrmModule.forFeature([clientPremiereEntity])],
    controllers: [ClientPremiereController],
    providers: [clientPremiereService]
  })
export class clientPremiereModule{}