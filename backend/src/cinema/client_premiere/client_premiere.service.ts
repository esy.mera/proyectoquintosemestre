import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Like, Repository } from "typeorm";
import { ChangeclientPremiereDto, } from "./DTO/Change-state-client_premiere.dto";
import { CreateclientPremiereDto } from "./DTO/CreateClient_premiere.dto";
import { UpdateclientPremiereDto } from "./DTO/UpdateClient_premiere.dto";
import { clientPremiereEntity } from "./client_premiere.entity";
import { globalAplicationDTO } from "src/global-dto/global-aplication.dto";

@Injectable()
export class clientPremiereService {
    constructor(@InjectRepository(clientPremiereEntity) private clientpremiereRepository: Repository<clientPremiereEntity>){}

    async find({limit,offset}:globalAplicationDTO){
        const clientpremiere: clientPremiereEntity[]= await this.clientpremiereRepository.find({
            relations:['idPremiere','idClient'],
            skip: limit * (offset - 1),
            take: limit,
            
        });
        return clientpremiere;
    }

    async findOne(id:number){
        const clientpremiereFind:clientPremiereEntity=await this.clientpremiereRepository.findOne({
            where:{id:id},
            relations:['idPremiere','idClient']
        })

        if(!clientpremiereFind){
            throw new  NotFoundException({
                message: "La premier del cliente no encontrado",
                error: "no encontrado"
            })
        }

        return clientpremiereFind;
    }

    async create(payload:CreateclientPremiereDto){
        const client_premiereCreate:clientPremiereEntity=this.clientpremiereRepository.create();
        client_premiereCreate.seat=payload.seat
        client_premiereCreate.idClient=payload.idClient
        client_premiereCreate.idPremiere=payload.idPremiere

        return await this.clientpremiereRepository.save(client_premiereCreate);
    }

    async Put(id:number,payload:UpdateclientPremiereDto){
        const clientpremiereFind:clientPremiereEntity=await this.findOne(id);
        if(!clientpremiereFind){
            throw new  NotFoundException({
                message: "La premier del cliente no encontrado",
                error: "no encontrado"
            })
        }

        const Updateclientpremier:UpdateclientPremiereDto={...payload}

        /*client_premiereCreate.seat=payload.seat
        client_premiereCreate.creationDate=payload.creationDate*/

        return await this.clientpremiereRepository.update(id, Updateclientpremier);
    }

    async changeState(id:number, payload:ChangeclientPremiereDto){
        const existingclientpremier:clientPremiereEntity = await this.clientpremiereRepository.findOne({
            where:{id:id}
        })

        if (!existingclientpremier) {
            throw new NotFoundException({
                message: "La premier del cliente no encontrado",
                error: "no encontrado"
            });
        }
    
        const updatedclientpremiere:clientPremiereEntity = { ...existingclientpremier, ...payload};
        
        return await this.clientpremiereRepository.update(id, updatedclientpremiere);
    }

    async delete(id:number){
        const clientpremiereFind= await this.findOne(id);

        if(!clientpremiereFind){
            throw new  NotFoundException({
                message: "La premier del cliente no encontrado",
                error: "no encontrado"
            })
        }

        return await this.clientpremiereRepository.delete(id)
    }
}