
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { PremiereEntity } from "../premiere/premiere.entity";
import { ClientEntity } from "../client/client.entity";

@Entity({name:"clientpremiere"})
export class clientPremiereEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({
        name:"seat",
        type: "varchar",
        length: "4"
    })
    seat: string;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

    @ManyToOne(() => PremiereEntity, clientpremiere => clientpremiere.clientpremiere, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }) 
    @JoinColumn({ name: "id_premiere" })
    idPremiere: PremiereEntity;


    @ManyToOne(() => ClientEntity, client => client.clientPremieres, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }) 
    @JoinColumn({ name: "id_client" })
    idClient: ClientEntity;
}