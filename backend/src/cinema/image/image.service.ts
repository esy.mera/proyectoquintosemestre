import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ImageEntity } from './image.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ImageService {
    constructor(
        @InjectRepository(ImageEntity)
        private readonly imageRepository: Repository<ImageEntity>,
    ) { }

    async find(){
        const images:ImageEntity[]= await this.imageRepository.find({
            relations:['movie']
        });
        
        return images;
    }

    async findOne(id:number){
        const findImage:ImageEntity=await this.imageRepository.findOne({
            where:{id:id},
            relations:['movie']
        })

        if(!findImage){
            throw new  NotFoundException({
                message: "image no encontrado",
                error: "no encontrado"
            })
        }

        


        return  findImage/*{
            name: findImage.name,
            data: findImage.data,
            movie: findImage.movie
        };*/
    }

    async create(imagen, nombre: string) {
        const newImagen = new ImageEntity();
        newImagen.name = nombre;
        newImagen.data = Buffer.from(imagen.buffer).toString('base64');

        const exito=await this.imageRepository.save(newImagen);

        return {response:'Imagen subida exitosamente.', id:exito.id, status:true };
    }

    async Put(id:number,image,nombre:string){
        const imageFind:ImageEntity=await this.imageRepository.findOne({
            where:{id:id},
            relations:['movie']
        });
        if(!imageFind){
            throw new  NotFoundException({
                message: "planeta no encontrado",
                error: "no encontrado"
            })
        }
        const newImagen = new ImageEntity();
        newImagen.name = nombre;
        newImagen.data = Buffer.from(image.buffer).toString('base64');



        return await this.imageRepository.update(id, newImagen);
    }


    async delete(id:number){
        const imageFind= await this.findOne(id);

        if(!imageFind){
            throw new  NotFoundException({
                message: "image no encontrado",
                error: "no encontrado"
            })
        }

        return await this.imageRepository.delete(id)
    }
}
