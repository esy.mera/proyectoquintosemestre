import { Entity, Column, PrimaryGeneratedColumn, OneToOne } from 'typeorm';
import { MovieEntity } from '../movie/movie.entity';

@Entity({name:"image"})
export class ImageEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ 
        name:"name", 
        type: 'varchar', 
        length: 100 
    })
    name: string;
  
    /*@Column({ type: 'bytea' })
    data: Buffer;*/
    @Column({ type: 'text' })
    data: string;

    @OneToOne(() => MovieEntity, movieentity => movieentity.idImage,{cascade:true})
    movie: MovieEntity


}