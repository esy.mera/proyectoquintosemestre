import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, UploadedFile, UseInterceptors } from '@nestjs/common';
import { ImageService } from './image.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('image')
@Controller('image')
export class ImageController {
    constructor(private readonly imageService: ImageService) {}

    @ApiOperation({ description: "find images", summary: "find images", })
    @Get()
    async find() {
        const images = await this.imageService.find();
        return images;
    }

    @ApiOperation({ description: "find One image", summary: "find One image", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        return await this.imageService.findOne(id);
    }

    @ApiOperation({ description: "add image", summary: "add image", })
    @Post()
    @UseInterceptors(FileInterceptor('image'))
    async subirImagen(@UploadedFile() image, @Body('name') name: string) {
        return this.imageService.create(image, name);
    }

    @ApiOperation({ description: "update image", summary: "update image", })
    @Put(':id')
    @UseInterceptors(FileInterceptor('image'))
    async update(@Param('id', ParseIntPipe) id: number, @UploadedFile() image, @Body('name') name: string) {
        return await this.imageService.Put(id,image,name);
    }



    @ApiOperation({ description: "delete image", summary: "delete image", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.imageService.delete(id);
    }
}
