import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query } from "@nestjs/common";
import { PremiereService } from "./premiere.service";
import { ApiOperation, ApiTags } from "@nestjs/swagger";
import { CreatePremiereDto } from "./DTO/CreatePremiere.dto";
import { UpdatePremiereDto } from "./DTO/UpdatePremiere.dto";
import { ChangePremiereDto } from "./DTO/Change-State-Premiere.dto";
import { globalAplicationDTO } from "src/global-dto/global-aplication.dto";

@ApiTags('premiere')
@Controller('premiere')
export class PremiereController {
    constructor(private readonly premiereService: PremiereService) {}

    @ApiOperation({ description: "find premiere", summary: "find planets", })
    @Get()
    async find(@Query() pagination:globalAplicationDTO) {
        return await this.premiereService.find(pagination)
    }

    @ApiOperation({ description: "find One premiere", summary: "find One planet", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        return await this.premiereService.findOne(id);
    }


    @ApiOperation({ description: "Create premiere", summary: "Create planet", })
    @Post()
    async create(@Body() payload: CreatePremiereDto) {
        return await this.premiereService.create(payload);
    }



    @ApiOperation({ description: "update premiere", summary: "update planet", })
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdatePremiereDto) {
        return await this.premiereService.Put(id,payload);
    }


    @ApiOperation({ description: "change premiere", summary: "change planet", })
    @Patch(':id')
    async changeState(@Param('id', ParseIntPipe) id: number, @Body() payload:ChangePremiereDto) {
        return await this.premiereService.changeState(id,payload);
    }


    @ApiOperation({ description: "delete preimere", summary: "delete planet", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.premiereService.delete(id);
    }
}