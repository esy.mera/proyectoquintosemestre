
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { MovieEntity } from "../movie/movie.entity";
import { clientPremiereEntity } from "../client_premiere/client_premiere.entity";
import { BranchEntity } from "../branch/branch.entity";

@Entity({name:"premiere"})
export class PremiereEntity{
    @PrimaryGeneratedColumn()
    id:number;

    @Column({
        name:"premieredate",
        type: "timestamp"
    })
    premiereDate: Date;

    @Column({
        name:"hour",
        type: "varchar",
        length: "10"
    })
    hour: string;

    @Column({
        name:"cinema",
        type: "integer",
    })
    cinema: number;

    @Column({
        name:"price",
        type: "float",
    })
    price: number;

    @Column({
        name:"total_tickets",
        type: "integer",
    })
    totalTickets: number;

    @Column({
        name:"reservation",
        type: "integer",
    })
    reservation: number;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

    @ManyToOne(() => MovieEntity, movie => movie.premieres, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }) 
    @JoinColumn({ name: "id_movie" })
    idMovie: MovieEntity;
    

    @OneToMany(() => clientPremiereEntity, clientpremiere => clientpremiere.idPremiere, { cascade: true })
    clientpremiere: clientPremiereEntity[]


    @ManyToOne(() =>  BranchEntity,  branch =>  branch.premieres, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }) 
    @JoinColumn({ name: "id_branch" })
    idBranch:  BranchEntity;
}