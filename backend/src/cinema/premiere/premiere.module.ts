import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PremiereEntity } from "./premiere.entity";
import { PremiereController } from "./premiere.controller";
import { PremiereService } from "./premiere.service";

@Module({
    imports: [TypeOrmModule.forFeature([PremiereEntity])],
    controllers:[PremiereController],
    providers:[PremiereService]
})
export class PremiereModule{}