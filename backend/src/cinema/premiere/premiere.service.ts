import { NotFoundException } from "@nestjs/common";
import { Injectable } from "@nestjs/common/decorators";
import { InjectRepository } from "@nestjs/typeorm";
import { PremiereEntity } from "./premiere.entity";
import { Like, Repository } from "typeorm";
import { CreatePremiereDto } from "./DTO/CreatePremiere.dto";
import { UpdatePremiereDto } from "./DTO/UpdatePremiere.dto";
import { ChangePremiereDto } from "./DTO/Change-State-Premiere.dto";
import { globalAplicationDTO } from "src/global-dto/global-aplication.dto";

@Injectable()
export class PremiereService {
    constructor(@InjectRepository(PremiereEntity) private premiereRepository: Repository<PremiereEntity>){}

    async find({offset=1,limit=50, search}:globalAplicationDTO){
        const premiere:PremiereEntity[]= await this.premiereRepository.find({
            relations:['idMovie', 'clientpremiere', 'idBranch','idMovie.idCategory','idMovie.idImage'],
            skip: limit * (offset - 1) ,
            take: limit,
            where: search
                ? [
                    {
                        idMovie: {
                            name:Like(`%${search}%`)
                        },
                    },
                ]
                : undefined,
        });
        return premiere;
    }

    async findOne(id:number){
        const PremiereFind:PremiereEntity=await this.premiereRepository.findOne({
            where:{id:id},
            relations:['idMovie', 'clientpremiere', 'idBranch']
        })

        if(!PremiereFind){
            throw new  NotFoundException({
                message: "Estreno no encontrado",
                error: "no encontrado"
            })
        }

        return PremiereFind;
    }

    async create(payload:CreatePremiereDto){
        const premiereCreate:PremiereEntity=this.premiereRepository.create();
        premiereCreate.premiereDate=payload.premiereDate
        premiereCreate.hour=payload.hour
        premiereCreate.cinema=payload.cinema
        premiereCreate.price=payload.price
        premiereCreate.totalTickets=payload.totalTickets
        premiereCreate.reservation=payload.reservation
        premiereCreate.idMovie=payload.idMovie
        premiereCreate.idBranch=payload.idBranch

        return await this.premiereRepository.save(premiereCreate);
    }

    async Put(id:number,payload:UpdatePremiereDto){
        const PremiereFind:PremiereEntity=await this.findOne(id);
        if(!PremiereFind){
            throw new  NotFoundException({
                message: "Estreno no encontrado",
                error: "no encontrado"
            })
        }

        const UpdatePremiere:UpdatePremiereDto={...payload}

        /* premiereCreate.premiereDate=payload.premiereDate
        premiereCreate.hour=payload.hour
        premiereCreate.cinema=payload.cinema
        premiereCreate.price=payload.price
        premiereCreate.totalTickets=payload.totalTickets
        premiereCreate.reservation=payload.reservation*/

        return await this.premiereRepository.update(id, UpdatePremiere);
    }

    async changeState(id:number, payload:ChangePremiereDto){
        const existingPremiere:PremiereEntity = await this.premiereRepository.findOne({
            where:{id:id}
        })

        if (!existingPremiere) {
            throw new NotFoundException({
                message: "Estreno no encontrado",
                error: "no encontrado"
            });
        }
    
        const updatedPremiere:PremiereEntity = { ...existingPremiere, ...payload};
        
        return await this.premiereRepository.update(id, updatedPremiere);
    }

    async delete(id:number){
        const PremiereFind= await this.findOne(id);

        if(!PremiereFind){
            throw new  NotFoundException({
                message: "Estreno no encontrado",
                error: "no encontrado"
            })
        }

        return await this.premiereRepository.delete(id)
    }
}