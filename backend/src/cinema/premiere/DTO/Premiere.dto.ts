import { Type } from "class-transformer";
import { IsDate, IsNotEmpty, IsNumber, IsString } from "class-validator";
import { BranchEntity } from "src/cinema/branch/branch.entity";
import { MovieEntity } from "src/cinema/movie/movie.entity";
import { IsNotEmptyValidationOption, IsDateValidationOption, IsStringValidationOption } from "src/common/messegeDTO";


export class premiereDto {

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsDate(IsDateValidationOption())
    @Type(() => Date)
    premiereDate: Date;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    hour: string;

    @IsNumber()
    @IsNotEmpty(IsNotEmptyValidationOption())
    cinema: number;

    @IsNumber()
    @IsNotEmpty(IsNotEmptyValidationOption())
    price: number;

    @IsNumber()
    @IsNotEmpty(IsNotEmptyValidationOption())
    totalTickets: number;

    @IsNumber()
    @IsNotEmpty(IsNotEmptyValidationOption())
    reservation: number;

    @IsNotEmpty(IsNotEmptyValidationOption())
    idBranch:  BranchEntity;

    @IsNotEmpty(IsNotEmptyValidationOption())
    idMovie: MovieEntity;
}
