import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { EmployeeEntity } from '../employee/employee.entity';
import { userDTO } from './DTO/user.dto';

@Injectable()
export class LoginService {
    constructor(@InjectRepository(EmployeeEntity) private EmployeeRepository: Repository<EmployeeEntity>){}

    async find({password,user}:userDTO){
        const Employee:EmployeeEntity[]= await this.EmployeeRepository.find({
            relations:['idBranch', 'idRole'],
            where:[
                    {
                        email: user,
                        password: password,
                    }
                ]

            
        });
        return Employee;
    }
}
