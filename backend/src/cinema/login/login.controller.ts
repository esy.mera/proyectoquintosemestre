import { Controller, Get, Query } from '@nestjs/common';
import { LoginService } from './login.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { userDTO } from './DTO/user.dto';

@ApiTags('Login')
@Controller('login')
export class LoginController {
    constructor(private readonly loginService:LoginService){}
    @ApiOperation({ description: "find employees", summary: "find employees", })
    @Get()
    async find(@Query() data:userDTO) {
        return await this.loginService.find(data)
    }
}
