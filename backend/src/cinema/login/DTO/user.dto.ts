import { IsEmail, IsNotEmpty, IsString } from "class-validator";
import { IsNotEmptyValidationOption } from "src/common/messegeDTO";


export class userDTO {
    @IsString()
    @IsNotEmpty(IsNotEmptyValidationOption())
   password:string;

   @IsEmail()
   @IsNotEmpty(IsNotEmptyValidationOption())
   user:string;
}