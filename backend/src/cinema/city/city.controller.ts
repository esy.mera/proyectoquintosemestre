import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query } from '@nestjs/common';
import { CityService } from './city.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateCityDTO } from './DTO/CreateCity.dto';
import { UpdateCityDTO } from './DTO/UpdateCity.dto';
import { ChangeCityDTO } from './DTO/change-state-city.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';


@ApiTags('city')
@Controller('city')
export class CityController {

    constructor(private readonly CityService:CityService){}

    @ApiOperation({ description: "find City", summary: "find City", })
    @Get()
    async find(@Query() pagination:globalAplicationDTO ) {
        return await this.CityService.find(pagination)
    }

    @ApiOperation({ description: "find One City", summary: "find One City", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        return await this.CityService.findOne(id);
    }


    @ApiOperation({ description: "Create City", summary: "Create City", })
    @Post()
    async create(@Body() payload: CreateCityDTO) {
        return await this.CityService.create(payload);
    }



    @ApiOperation({ description: "update City", summary: "update City", })
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdateCityDTO) {
        return await this.CityService.Put(id,payload);
    }


    @ApiOperation({ description: "change City", summary: "change City", })
    @Patch(':id')
    async changeState(@Param('id', ParseIntPipe) id: number, @Body() payload:ChangeCityDTO) {
        return await this.CityService.changeState(id,payload);
    }


    @ApiOperation({ description: "delete City", summary: "delete City", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.CityService.delete(id);
    }
}
