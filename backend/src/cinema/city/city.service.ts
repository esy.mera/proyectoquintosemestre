import { Injectable, NotFoundException } from '@nestjs/common';
import { CityEntity } from './city.entity';
import { Like, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateCityDTO } from './DTO/CreateCity.dto';
import { UpdateCityDTO } from './DTO/UpdateCity.dto';
import { ChangeCityDTO } from './DTO/change-state-city.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';

@Injectable()
export class CityService {
    constructor(@InjectRepository(CityEntity) private cityRepository: Repository<CityEntity>){}

    async find({offset=1,limit=50, search}:globalAplicationDTO){
        const city:CityEntity[]= await this.cityRepository.find({
            relations:['branches'],
            skip: limit * (offset - 1),
            take: limit,
            where: search
                ? [
                    {
                        description: Like(`%${search}%`),
                    },
                ]
                : undefined,
        });
        return city;
    }

    async findOne(id:number){
        const findCity:CityEntity=await this.cityRepository.findOne({
            where:{id:id},
            relations:['branches']
        })

        if(!findCity){
            throw new  NotFoundException({
                message: "city no encontrado",
                error: "no encontrado"
            })
        }

        return findCity;
    }

    async create(payload:CreateCityDTO){
        const cityCreate:CityEntity=this.cityRepository.create();
        cityCreate.description=payload.description
        return await this.cityRepository.save(cityCreate);
    }

    async Put(id:number,payload:UpdateCityDTO){
        const cityFind:CityEntity=await this.findOne(id);
        if(!cityFind){
            throw new  NotFoundException({
                message: "city no encontrado",
                error: "no encontrado"
            })
        }

        const UpdateCity:UpdateCityDTO={...payload}

        UpdateCity.description=payload.description
        
       

        return await this.cityRepository.update(id, UpdateCity);
    }

    async changeState(id:number, payload:ChangeCityDTO){
        const existingCity:CityEntity = await this.cityRepository.findOne({
            where:{id:id}
        })

        if (!existingCity) {
          throw new NotFoundException({
            message: "City no encontrado",
            error: "no encontrado"
        });
        }
    
        const updatedCity:CityEntity = { ...existingCity, ...payload};
        
        return await this.cityRepository.update(id, updatedCity);
    }

    async delete(id:number){
        const cityFind= await this.findOne(id);

        if(!cityFind){
            throw new  NotFoundException({
                message: "City no encontrado",
                error: "no encontrado"
            })
        }

        return await this.cityRepository.delete(id)
    }
}
