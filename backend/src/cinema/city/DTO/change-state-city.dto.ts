import { PartialType } from "@nestjs/swagger";
import { cityDTO } from "./City.dto";

export class ChangeCityDTO extends PartialType(cityDTO){
    
}