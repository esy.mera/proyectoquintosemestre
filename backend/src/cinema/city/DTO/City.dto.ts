import { IsNotEmpty, IsString } from "class-validator";
import {  IsNotEmptyValidationOption, IsStringValidationOption } from "src/common/messegeDTO";

export class cityDTO {
    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    description: string;

}