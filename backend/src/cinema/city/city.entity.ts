
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { BranchEntity } from '../branch/branch.entity';

@Entity({name:"city"})
export class CityEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({
        name:"description",
        type: "varchar",
        length: "100"
    })
    description: string;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

    @OneToMany(() => BranchEntity , branch => branch.idCity, { cascade: true })
    branches: BranchEntity[]
   
}