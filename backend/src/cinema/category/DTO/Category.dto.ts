import { Type } from "class-transformer";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import { IsNumberValidationOption, IsNotEmptyValidationOption, IsStringValidationOption } from "src/common/messegeDTO";

export class categoryDTO {
    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    description: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsNumber({},IsNumberValidationOption())
    @Type(() => Number)
    recommendedAge: Number;   
}