import { PartialType } from "@nestjs/swagger";
import { categoryDTO } from "./Category.dto";

export class ChangeCategoryDTO extends PartialType(categoryDTO){
    
}