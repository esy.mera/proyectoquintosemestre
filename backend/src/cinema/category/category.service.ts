import { Injectable, NotFoundException } from '@nestjs/common';
import { CategoryEntity } from './category.entity';
import { Like, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateCategoryDTO } from './DTO/CreateCategory.dto';
import { UpdateCategoryDTO } from './DTO/UpdateCategory.dto';
import { ChangeCategoryDTO } from './DTO/change-state-category.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';

@Injectable()
export class CategoryService {
    constructor(@InjectRepository(CategoryEntity) private categoryRepository: Repository<CategoryEntity>) { }

    async find({ offset=1,limit=50, search }: globalAplicationDTO ) {
        const category: CategoryEntity[] = await this.categoryRepository.find({
            relations: ['movies'],
            skip: limit * (offset - 1),
            take: limit,
            where: search
                ? [
                    {
                        description: Like(`%${search}%`),
                    },
                ]
                : undefined,
        });
        return category;
    }

    async findOne(id: number) {
        const findCategory: CategoryEntity = await this.categoryRepository.findOne({
            where: { id: id },
            relations: ['movies']
        })

        if (!findCategory) {
            throw new NotFoundException({
                message: "category no encontrado",
                error: "no encontrado"
            })
        }

        return findCategory;
    }

    async create(payload: CreateCategoryDTO) {
        const categoryCreate: CategoryEntity = this.categoryRepository.create();
        categoryCreate.description = payload.description
        categoryCreate.recommendedAge = payload.recommendedAge


        return await this.categoryRepository.save(categoryCreate);
    }

    async Put(id: number, payload: UpdateCategoryDTO) {
        const categoryFind: CategoryEntity = await this.findOne(id);
        if (!categoryFind) {
            throw new NotFoundException({
                message: "category no encontrado",
                error: "no encontrado"
            })
        }

        const UpdateCategory: UpdateCategoryDTO = { ...payload }

        UpdateCategory.description = payload.description
        UpdateCategory.recommendedAge = payload.recommendedAge


        return await this.categoryRepository.update(id, UpdateCategory);
    }

    async changeState(id: number, payload: ChangeCategoryDTO) {
        const existingCategory: CategoryEntity = await this.categoryRepository.findOne({
            where: { id: id }
        })

        if (!existingCategory) {
            throw new NotFoundException({
                message: "category no encontrado",
                error: "no encontrado"
            });
        }

        const updatedCategory: CategoryEntity = { ...existingCategory, ...payload };

        return await this.categoryRepository.update(id, updatedCategory);
    }

    async delete(id: number) {
        const categoryFind = await this.findOne(id);

        if (!categoryFind) {
            throw new NotFoundException({
                message: "category no encontrado",
                error: "no encontrado"
            })
        }

        return await this.categoryRepository.delete(id)
    }
}
