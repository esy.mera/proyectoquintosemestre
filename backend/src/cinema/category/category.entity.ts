
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { MovieEntity } from '../movie/movie.entity';

@Entity({name:"category"})
export class CategoryEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({
        name:"description",
        type: "varchar",
        length: "100"
    })
    description: string;

    @Column({
        name:"recommend_age",
        type: "integer"
    })
    recommendedAge: Number;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

    @OneToMany(() => MovieEntity, movieentity => movieentity.idCategory, { cascade: true })
    movies: MovieEntity[]

}