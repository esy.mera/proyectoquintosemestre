import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query } from '@nestjs/common';
import { CategoryService } from './category.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateCategoryDTO } from './DTO/CreateCategory.dto';
import { UpdateCategoryDTO } from './DTO/UpdateCategory.dto';
import { ChangeCategoryDTO } from './DTO/change-state-category.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';

@ApiTags('category')
@Controller('category')
export class CategoryController {

    constructor(private readonly CategoryService:CategoryService){}

    @ApiOperation({ description: "find category", summary: "find category", })
    @Get()
    async find(@Query() pagination:globalAplicationDTO) {
        return await this.CategoryService.find(pagination)
    }

    @ApiOperation({ description: "find One category", summary: "find One category", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        return await this.CategoryService.findOne(id);
    }


    @ApiOperation({ description: "Create category", summary: "Create category", })
    @Post()
    async create(@Body() payload: CreateCategoryDTO) {
        return await this.CategoryService.create(payload);
    }



    @ApiOperation({ description: "update category", summary: "update category", })
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdateCategoryDTO) {
        return await this.CategoryService.Put(id,payload);
    }


    @ApiOperation({ description: "change category", summary: "change category", })
    @Patch(':id')
    async changeState(@Param('id', ParseIntPipe) id: number, @Body() payload:ChangeCategoryDTO) {
        return await this.CategoryService.changeState(id,payload);
    }


    @ApiOperation({ description: "delete category", summary: "delete category", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.CategoryService.delete(id);
    }
}
