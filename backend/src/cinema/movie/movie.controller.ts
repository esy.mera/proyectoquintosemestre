import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query } from '@nestjs/common';
import { MovieService } from './movie.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateMovieDTO } from './DTO/CreateMovie.dto';
import { UpdateMovieDTO } from './DTO/UpdateMovie.dto copy';
import { ChangeMovieDTO } from './DTO/change-state-movie.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';

@ApiTags('movie')
@Controller('movie')
export class MovieController {

    constructor(private readonly MovieService:MovieService){}

    @ApiOperation({ description: "find Movies", summary: "find Movies", })
    @Get()
    async find(@Query() pagination:globalAplicationDTO) {
        let movies:any = await this.MovieService.find(pagination)
        movies.forEach(e=>{
            e.idImage={
                id: e.idImage.id,
                name:e.idImage.name,
                data:e.idImage.data.toString('base64')
            }
        })
        return movies
    }

    @ApiOperation({ description: "find One Movie", summary: "find One Movie", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        let movie:any = await this.MovieService.findOne(id)
            movie.idImage={
                id: movie.idImage.id,
                name:movie.idImage.name,
                data:movie.idImage.data.toString('base64')
            }
        return movie
    }


    @ApiOperation({ description: "Create Movie", summary: "Create Movie", })
    @Post()
    async create(@Body() payload: CreateMovieDTO) {
        return await this.MovieService.create(payload);
    }



    @ApiOperation({ description: "update Movie", summary: "update Movie", })
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdateMovieDTO) {
        return await this.MovieService.Put(id,payload);
    }


    @ApiOperation({ description: "change Movie", summary: "change Movie", })
    @Patch(':id')
    async changeState(@Param('id', ParseIntPipe) id: number, @Body() payload:ChangeMovieDTO) {
        return await this.MovieService.changeState(id,payload);
    }


    @ApiOperation({ description: "delete Movie", summary: "delete Movie", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.MovieService.delete(id);
    }
}
