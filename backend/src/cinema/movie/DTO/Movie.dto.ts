import { Type } from "class-transformer";
import { IsDate, IsNotEmpty,IsString, MaxLength} from "class-validator";
import { CategoryEntity } from "src/cinema/category/category.entity";
import { ImageEntity } from "src/cinema/image/image.entity";

import { IsDateValidationOption, IsNotEmptyValidationOption,IsStringValidationOption, maxLengthValidationOption } from "src/common/messegeDTO";


export class movieDTO {
    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    name: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsDate(IsDateValidationOption())
    @Type(() => Date)
    year: Date;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    director: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    casting: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    synopsis: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    @MaxLength(4,maxLengthValidationOption())
    duration: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    rating: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    productionStudio: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    country: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    language: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    format: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    idImage: ImageEntity;

    @IsNotEmpty(IsNotEmptyValidationOption())
    idCategory: CategoryEntity;


    
}