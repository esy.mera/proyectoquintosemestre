import { PartialType } from "@nestjs/swagger";
import { movieDTO } from "./Movie.dto";

export class ChangeMovieDTO extends PartialType(movieDTO){
    
}