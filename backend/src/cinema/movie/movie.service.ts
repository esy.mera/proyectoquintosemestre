import { Injectable, NotFoundException } from '@nestjs/common';
import { MovieEntity } from './movie.entity';
import { Like, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMovieDTO } from './DTO/CreateMovie.dto';
import { UpdateMovieDTO } from './DTO/UpdateMovie.dto copy';
import { ChangeMovieDTO } from './DTO/change-state-movie.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';

@Injectable()
export class MovieService {
    constructor(@InjectRepository(MovieEntity) private movieRepository: Repository<MovieEntity>){}

    async find({limit, offset, search}:globalAplicationDTO){
        const movies:MovieEntity[]= await this.movieRepository.find({
            relations:['idCategory','idImage','premieres'],
            skip: limit * (offset - 1),
            take: limit,
            where: search
                ? [
                    {
                        name: Like(`%${search}%`),
                    },
                ]
                : undefined,
        });
        
        return movies;
    }

    async findOne(id:number){
        const findMovie:MovieEntity=await this.movieRepository.findOne({
            where:{id:id},
            relations:['idCategory','idImage','premieres']
        })

        if(!findMovie){
            throw new  NotFoundException({
                message: "pelicula no encontrado",
                error: "no encontrado"
            })
        }

        return findMovie;
    }

    async create(payload:CreateMovieDTO){
        const movieCreate:MovieEntity=this.movieRepository.create();
        movieCreate.name=payload.name
        movieCreate.casting=payload.casting
        movieCreate.country=payload.country
        movieCreate.director=payload.director
        movieCreate.duration=payload.duration
        movieCreate.format=payload.format
        movieCreate.language=payload.language
        movieCreate.productionStudio=payload.productionStudio
        movieCreate.rating=payload.rating
        movieCreate.synopsis=payload.synopsis
        movieCreate.year=payload.year
        movieCreate.idImage=payload.idImage
        movieCreate.idCategory=payload.idCategory

        return await this.movieRepository.save(movieCreate);
    }

    async Put(id:number,payload:UpdateMovieDTO){
        const MovieFind:MovieEntity=await this.findOne(id);
        if(!MovieFind){
            throw new  NotFoundException({
                message: "pelicula no encontrado",
                error: "no encontrado"
            })
        }

        const UpdateMovie:UpdateMovieDTO={...payload}


        return await this.movieRepository.update(id, UpdateMovie);
    }

    async changeState(id:number, payload:ChangeMovieDTO){
        const existingMovie:MovieEntity = await this.movieRepository.findOne({
            where:{id:id}
        });
        

        if (!existingMovie) {
          throw new NotFoundException({
            message: "pelicula no encontrado",
            error: "no encontrado"
        });
        }
    
        const updatedMovie:MovieEntity = { ...existingMovie, ...payload};
        console.log(updatedMovie)
        
        return await this.movieRepository.update(id, updatedMovie);
    }

    async delete(id:number){
        const MovieFind= await this.findOne(id);

        if(!MovieFind){
            throw new  NotFoundException({
                message: "pelicula no encontrado",
                error: "no encontrado"
            })
        }

        return await this.movieRepository.delete(id)
    }
}
