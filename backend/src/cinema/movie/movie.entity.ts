import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany, OneToOne, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { ImageEntity } from '../image/image.entity';
import { CategoryEntity } from '../category/category.entity';
import { PremiereEntity } from '../premiere/premiere.entity';


@Entity({name:"movie"})
export class MovieEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({
        name:"name",
        type: "varchar",
        length: "100"
    })
    name: string;

    @Column({
        name:"year",
        type: "timestamp"
    })
    year: Date;

    @Column({
        name:"director",
        type: "varchar",
        length: "100"
    })
    director: string;

    @Column({
        name:"casting",
        type: "text",
    })
    casting: string;

    @Column({
        name:"synopsis",
        type: "text",
    })
    synopsis: string;

    @Column({
        name:"duration",
        type: "varchar",
        length: "100"
    })
    duration: string;

    @Column({
        name:"rating",
        type: "varchar",
        length: "100"
    })
    rating: string;


    @Column({
        name:"production_studio",
        type: "varchar",
        length: "100"
    })
    productionStudio: string;

    @Column({
        name:"country",
        type: "varchar",
        length: "100"
    })
    country: string;

    @Column({
        name:"language",
        type: "varchar",
        length: "100"
    })
    language: string;

    @Column({
        name:"format",
        type: "varchar",
        length: "100"
    })
    format: string;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date


    @OneToOne(() => ImageEntity, image => image.movie, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      })
    @JoinColumn({ name: "id_image" })
    idImage: ImageEntity;


    @ManyToOne(() => CategoryEntity, image => image.movies, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }) 
    @JoinColumn({ name: "id_category" })
    idCategory: CategoryEntity;

    @OneToMany(() => PremiereEntity, premiere =>  premiere.idMovie, { cascade: true })
    premieres: PremiereEntity[]



}