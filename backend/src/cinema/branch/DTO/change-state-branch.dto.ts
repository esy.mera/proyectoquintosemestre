import { PartialType } from "@nestjs/swagger";
import { branchDTO } from "./Branch.dto";

export class ChangeBranchDTO extends PartialType(branchDTO){
    
}