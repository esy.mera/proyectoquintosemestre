import { Type } from "class-transformer";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import { CityEntity } from "src/cinema/city/city.entity";

import { IsNumberValidationOption, IsNotEmptyValidationOption, IsStringValidationOption } from "src/common/messegeDTO";

export class branchDTO {
    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    name: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    address: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsNumber({},IsNumberValidationOption())
    @Type(() => Number)
    phone: Number;  
    
    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsNumber({},IsNumberValidationOption())
    @Type(() => Number)
    peopleCapacity: Number;  

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    schedule: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    idCity: CityEntity;
}