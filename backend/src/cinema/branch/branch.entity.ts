
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { PremiereEntity } from '../premiere/premiere.entity';
import { CityEntity } from '../city/city.entity';
import { EmployeeEntity } from '../employee/employee.entity';

@Entity({name:"branch"})
export class BranchEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({
        name:"name",
        type: "varchar",
        length: "50"
    })
    name: string;

    @Column({
        name:"address",
        type: "varchar",
        length: "50"
    })
    address: string;

    @Column({
        name:"phone",

        type: "integer",
        
    })
    phone: Number;

    @Column({
        name:"people_capacity",
        type: "integer",
    })
    peopleCapacity: Number;

    @Column({
        name:"schedule",
        type: "varchar",
    })
    schedule: string;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

    @OneToMany(() => PremiereEntity, premiere => premiere.idBranch, { cascade: true })
    premieres: PremiereEntity[]

    @ManyToOne(() =>   CityEntity ,  city =>  city.branches, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }) 
    @JoinColumn({ name: "id_city" })
    idCity: CityEntity;


    @OneToMany(() => EmployeeEntity, employe => employe.idBranch, { cascade: true })
    employes: EmployeeEntity[]
}