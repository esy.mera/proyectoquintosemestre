import { Injectable, NotFoundException } from '@nestjs/common';
import { BranchEntity } from './branch.entity';
import { Like, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateBranchDTO } from './DTO/CreateBranch.dto';
import { UpdateBranchDTO } from './DTO/UpdateBranch.dto';
import { ChangeBranchDTO } from './DTO/change-state-branch.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';

@Injectable()
export class BranchService {
    constructor(@InjectRepository(BranchEntity) private branchRepository: Repository<BranchEntity>){}

    async find({offset=1,limit=50, search}:globalAplicationDTO){
        const branch:BranchEntity[]= await this.branchRepository.find({
            relations:['premieres','idCity', 'employes'],
            skip: limit * (offset - 1),
            take: limit,
            where: search
                ? [
                    {
                        name: Like(`%${search}%`),
                    },
                ]
                : undefined,
        });
        return branch;
    }

    async findOne(id:number){
        const findBranch:BranchEntity=await this.branchRepository.findOne({
            where:{id:id},
            relations:['premieres','idCity', 'employes']
        })

        if(!findBranch){
            throw new  NotFoundException({
                message: "branch no encontrado",
                error: "no encontrado"
            })
        }

        return findBranch;
    }

    async create(payload:CreateBranchDTO){
        const branchCreate:BranchEntity=this.branchRepository.create();
        branchCreate.name=payload.name
        branchCreate.address=payload.address
        branchCreate.phone=payload.phone
        branchCreate.peopleCapacity=payload.peopleCapacity
        branchCreate.schedule=payload.schedule
        branchCreate.idCity=payload.idCity
        

        return await this.branchRepository.save(branchCreate);
    }

    async Put(id:number,payload:UpdateBranchDTO){
        const branchFind:BranchEntity=await this.findOne(id);
        if(!branchFind){
            throw new  NotFoundException({
                message: "branch no encontrado",
                error: "no encontrado"
            })
        }

        const UpdateBranch:UpdateBranchDTO={...payload}

        UpdateBranch.name=payload.name
        UpdateBranch.address=payload.address
        UpdateBranch.phone=payload.phone
        UpdateBranch.peopleCapacity=payload.peopleCapacity
        UpdateBranch.schedule=payload.schedule
       

        return await this.branchRepository.update(id, UpdateBranch);
    }

    async changeState(id:number, payload:ChangeBranchDTO){
        console.log(payload)
        const existingBranch:BranchEntity = await this.branchRepository.findOne({
            where:{id:id}
        })

        if (!existingBranch) {
          throw new NotFoundException({
            message: "branch no encontrado",
            error: "no encontrado"
        });
        }
    
        const updatedBranch:BranchEntity = { ...existingBranch, ...payload};
        
        return await this.branchRepository.update(id, updatedBranch);
    }

    async delete(id:number){
        const branchFind= await this.findOne(id);

        if(!branchFind){
            throw new  NotFoundException({
                message: "branch no encontrado",
                error: "no encontrado"
            })
        }

        return await this.branchRepository.delete(id)
    }
}
