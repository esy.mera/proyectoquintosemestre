import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query } from '@nestjs/common';
import { BranchService } from './branch.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateBranchDTO } from './DTO/CreateBranch.dto';
import { UpdateBranchDTO } from './DTO/UpdateBranch.dto';
import { ChangeBranchDTO } from './DTO/change-state-branch.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';

@ApiTags('branch')
@Controller('branch')
export class BranchController {

    constructor(private readonly BranchService:BranchService){}

    @ApiOperation({ description: "find Branch", summary: "find Branch", })
    @Get()
    async find(@Query() pagination:globalAplicationDTO) {
        return await this.BranchService.find(pagination)
    }

    @ApiOperation({ description: "find One Branch", summary: "find One Branch", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        return await this.BranchService.findOne(id);
    }


    @ApiOperation({ description: "Create Branch", summary: "Create Branch", })
    @Post()
    async create(@Body() payload: CreateBranchDTO) {
        return await this.BranchService.create(payload);
    }



    @ApiOperation({ description: "update Branch", summary: "update Branch", })
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdateBranchDTO) {
        return await this.BranchService.Put(id,payload);
    }


    @ApiOperation({ description: "change Branch", summary: "change Branch", })
    @Patch(':id')
    async changeState(@Param('id', ParseIntPipe) id: number, @Body() payload:ChangeBranchDTO) {
        return await this.BranchService.changeState(id,payload);
    }


    @ApiOperation({ description: "delete Branch", summary: "delete Branch", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.BranchService.delete(id);
    }
}
