
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { BranchEntity } from '../branch/branch.entity';
import { roleEntity } from '../roles/role.entity';

@Entity({name:"employee"})
export class EmployeeEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({
        name:"name",
        type: "varchar",
        length: "80"
    })
    name: string;

    @Column({
        name:"contract_date",
        type: "timestamp"
    })
    contractDate: Date;

    @Column({
        name:"salary",
        type: "float"
    })
    salary: string;

    @Column({
        name:"email",
        type: "varchar",
        length: "100",
        unique: true
    })
    email: string;

    @Column({
        name:"phone",
        type: "varchar",
        length: "15"
    })
    phone: string;

    @Column({
        name:"address",
        type: "varchar"
    })
    address: string;

    @Column({
        name:"password",
        type: "varchar",
        length: "100"
    })
    password: string;

    @Column({
        name:"civil",
        type: "varchar",
        length: "25"
    })
    civil: string;

    @Column({
        name:"birthdate",
        type: "timestamp"
    })
    birthdate: Date;

    @Column({
        name:"gender",
        type: "varchar",
        length: "25"
    })
    gender: string;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

    @ManyToOne(() => BranchEntity ,  branch =>  branch.employes, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }) 
    @JoinColumn({ name: "id_branch" })
    idBranch:  BranchEntity;


    @ManyToOne(() => roleEntity ,  role =>  role.employes, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      }) 
    @JoinColumn({ name: "id_role" })
    idRole:  roleEntity;



}