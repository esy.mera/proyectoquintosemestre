import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Put, Query } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { EmployeeService } from './employee.service';
import { CreateEmployeeDTO } from './DTO/CreateEmployee.dto';
import { UpdateEmployeeDTO } from './DTO/UpdateEmployee.dto copy';
import { ChangeEmployeeDTO } from './DTO/change-state-employee.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';

@ApiTags('Employee')
@Controller('employee')
export class EmployeeController {
    constructor(private readonly EmployeeService:EmployeeService){}

    @ApiOperation({ description: "find employees", summary: "find employees", })
    @Get()
    async find(@Query() pagination:globalAplicationDTO) {
        return await this.EmployeeService.find(pagination)
    }

    @ApiOperation({ description: "find One employee", summary: "find One employee", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        return await this.EmployeeService.findOne(id);
    }


    @ApiOperation({ description: "Create employee", summary: "Create employee", })
    @Post()
    async create(@Body() payload: CreateEmployeeDTO) {
        return await this.EmployeeService.create(payload);
    }



    @ApiOperation({ description: "update employee", summary: "update employee", })
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdateEmployeeDTO) {
        return await this.EmployeeService.Put(id,payload);
    }


    @ApiOperation({ description: "change employee", summary: "change employee", })
    @Patch(':id')
    async changeState(@Param('id', ParseIntPipe) id: number, @Body() payload:ChangeEmployeeDTO) {
        return await this.EmployeeService.changeState(id,payload);
    }


    @ApiOperation({ description: "delete employee", summary: "delete employee", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.EmployeeService.delete(id);
    }
}
