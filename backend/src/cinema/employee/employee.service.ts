import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EmployeeEntity } from './employee.entity';
import { Like, Repository } from 'typeorm';
import { CreateEmployeeDTO } from './DTO/CreateEmployee.dto';
import { UpdateEmployeeDTO } from './DTO/UpdateEmployee.dto copy';
import { ChangeEmployeeDTO } from './DTO/change-state-employee.dto';
import { globalAplicationDTO } from 'src/global-dto/global-aplication.dto';

@Injectable()
export class EmployeeService {
    constructor(@InjectRepository(EmployeeEntity) private EmployeeRepository: Repository<EmployeeEntity>){}

    async find({offset=1,limit=50, search}:globalAplicationDTO){
        const Employee:EmployeeEntity[]= await this.EmployeeRepository.find({
            relations:['idBranch', 'idRole'],
            skip: limit * (offset - 1),
            take: limit,
            where: search
                ? [
                    {
                        name: Like(`%${search}%`),
                    },
                    {
                        email: Like(`%${search}%`),
                    },
                ]
                : undefined,
        });
        return Employee;
    }

    async findOne(id:number){
        const findEmployee:EmployeeEntity=await this.EmployeeRepository.findOne({
            where:{id:id},
            relations:['idBranch', 'idRole']
        })

        if(!findEmployee){
            throw new  NotFoundException({
                message: "planeta no encontrado",
                error: "no encontrado"
            })
        }

        return findEmployee;
    }

    async create(payload:CreateEmployeeDTO){
        const employeeCreate:EmployeeEntity=this.EmployeeRepository.create();
        employeeCreate.name=payload.name
        employeeCreate.contractDate=payload.contractDate
        employeeCreate.salary=payload.salary
        employeeCreate.email=payload.email
        employeeCreate.phone=payload.phone
        employeeCreate.address=payload.address
        employeeCreate.password=payload.password
        employeeCreate.civil=payload.civil
        employeeCreate.birthdate=payload.birthdate
        employeeCreate.gender=payload.gender
        employeeCreate.idBranch=payload.idBranch
        employeeCreate.idRole=payload.idRole

        return await this.EmployeeRepository.save(employeeCreate);
    }

    async Put(id:number,payload:UpdateEmployeeDTO){
        const EmployeeFind:EmployeeEntity=await this.findOne(id);
        if(!EmployeeFind){
            throw new  NotFoundException({
                message: "empleado no encontrado",
                error: "no encontrado"
            })
        }

        const UpdateEmployee:UpdateEmployeeDTO={...payload}


        return await this.EmployeeRepository.update(id, UpdateEmployee);
    }

    async changeState(id:number, payload:ChangeEmployeeDTO){
        const existingEmployee:EmployeeEntity = await this.EmployeeRepository.findOne({
            where:{id:id}
        })

        if (!existingEmployee) {
          throw new NotFoundException({
            message: "empleado no encontrado",
            error: "no encontrado"
        });
        }
    
        const updatedEmployee:EmployeeEntity = { ...existingEmployee, ...payload};
        
        return await this.EmployeeRepository.update(id, updatedEmployee);
    }

    async delete(id:number){
        const EmployeeFind= await this.findOne(id);

        if(!EmployeeFind){
            throw new  NotFoundException({
                message: "empleado no encontrado",
                error: "no encontrado"
            })
        }

        return await this.EmployeeRepository.delete(id)
    }
}
