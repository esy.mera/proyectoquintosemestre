import { PartialType } from "@nestjs/swagger";
import { employeeDTO} from "./Employee.dto";

export class ChangeEmployeeDTO extends PartialType(employeeDTO){
    
}