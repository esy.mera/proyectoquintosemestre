import { Type } from "class-transformer";
import { IsDate, IsDecimal, IsEmail, IsNotEmpty, IsPhoneNumber, IsString, MaxLength } from "class-validator";
import { BranchEntity } from "src/cinema/branch/branch.entity";
import { roleEntity } from "src/cinema/roles/role.entity";
import { IsDateValidationOption, IsEmailValidationOption, IsNotEmptyValidationOption, IsPhoneNumberValidationOption, IsStringValidationOption, maxLengthValidationOption } from "src/common/messegeDTO";

export class employeeDTO {
    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    name: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsDate(IsDateValidationOption())
    @Type(() => Date)
    contractDate: Date;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsDecimal({ })
    salary: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsEmail({}, IsEmailValidationOption())
    email: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsPhoneNumber('US',IsPhoneNumberValidationOption())
    phone: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString()
    address: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString()
    password: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString()
    @MaxLength(25, maxLengthValidationOption())
    civil: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsDate(IsDateValidationOption())
    @Type(() => Date)
    birthdate: Date;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString()
    @MaxLength(25, maxLengthValidationOption())
    gender: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    idBranch:  BranchEntity;

    @IsNotEmpty(IsNotEmptyValidationOption())
    idRole:  roleEntity;

    
}