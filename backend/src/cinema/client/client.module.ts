import { Module } from "@nestjs/common";
import { ClientEntity } from "./client.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ClientController } from "./client.controller";
import { ClientService } from "./client.service";

@Module({
    imports: [TypeOrmModule.forFeature([ClientEntity])],
    controllers:[ClientController],
    providers:[ClientService]
})
export class ClientModule {}