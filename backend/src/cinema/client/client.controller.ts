import { Controller, Get, Param, Post, Body, Put, ParseIntPipe, Patch, Delete, Query } from "@nestjs/common";
import { ApiTags, ApiOperation } from "@nestjs/swagger";
import { ClientService } from "./client.service";
import { CreateClientDto } from "./DTO/createClient.dto";
import { UpdateClientDto } from "./DTO/updateClient.dto";
import { ChangeClientDto } from "./DTO/change-state-client.dto";
import { globalAplicationDTO } from "src/global-dto/global-aplication.dto";

@ApiTags('client')
@Controller('client')
export class ClientController {

    constructor(private readonly ClientService:ClientService){}

    @ApiOperation({ description: "find client", summary: "find client", })
    @Get()
    async find(@Query() pagination:globalAplicationDTO) {
        return await this.ClientService.find(pagination)
    }

    @ApiOperation({ description: "find One client", summary: "find One client", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        return await this.ClientService.findOne(id);
    }


    @ApiOperation({ description: "Create client", summary: "Create client", })
    @Post()
    async create(@Body() payload: CreateClientDto) {
        return await this.ClientService.create(payload);
    }



    @ApiOperation({ description: "update client", summary: "update client", })
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdateClientDto) {
        return await this.ClientService.Put(id,payload);
    }


    @ApiOperation({ description: "change client", summary: "change client", })
    @Patch(':id')
    async changeState(@Param('id', ParseIntPipe) id: number, @Body() payload:ChangeClientDto) {
        return await this.ClientService.changeState(id,payload);
    }


    @ApiOperation({ description: "delete client", summary: "delete client", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.ClientService.delete(id);
    }
}