import { PartialType } from "@nestjs/swagger";
import { clientDto } from "./client.dto";

export class ChangeClientDto extends PartialType (clientDto){
    
}