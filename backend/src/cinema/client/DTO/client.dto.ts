import { Type } from "class-transformer";
import { IsDate, IsEmail, IsNotEmpty, IsNumber, IsPhoneNumber, IsString, Length } from "class-validator";
import { IsNotEmptyValidationOption, IsStringValidationOption, IsDateValidationOption } from "src/common/messegeDTO";

export class clientDto {

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    identificationCard: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    names: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    @IsEmail()
    email: string;

    @IsPhoneNumber('EC')
    @Length(13,13)
    phone: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsString(IsStringValidationOption())
    gender: string;

    @IsNotEmpty(IsNotEmptyValidationOption())
    @IsNumber()
    age: number;
}