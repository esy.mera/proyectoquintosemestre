import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Like, Repository } from "typeorm";
import { ClientEntity } from "./client.entity";
import { CreateClientDto } from "./DTO/createClient.dto";
import { UpdateClientDto } from "./DTO/updateClient.dto";
import { ChangeClientDto } from "./DTO/change-state-client.dto";
import { globalAplicationDTO } from "src/global-dto/global-aplication.dto";

@Injectable()
export class ClientService {
    constructor(@InjectRepository(ClientEntity) private clientRepository: Repository<ClientEntity>){}

    async find({offset=1,limit=50, search}:globalAplicationDTO){
        const client: ClientEntity[]= await this.clientRepository.find({
            relations:['clientPremieres'],
            skip: limit * (offset - 1),
            take: limit,
            where: search
                ? [
                    {
                        names: Like(`%${search}%`),
                    },
                ]
                : undefined,
        });
        return client;
    }

    async findOne(id:number){
        const clientFind:ClientEntity=await this.clientRepository.findOne({
            where:{id:id},
            relations:['clientPremieres']
        })

        if(!clientFind){
            throw new  NotFoundException({
                message: "Cliente no encontrado",
                error: "no encontrado"
            })
        }

        return clientFind;
    }

    async create(payload:CreateClientDto){
        const clientCreate:ClientEntity=this.clientRepository.create();
        clientCreate.identificationCard=payload.identificationCard
        clientCreate.names=payload.names
        clientCreate.email=payload.email
        clientCreate.phone=payload.phone
        clientCreate.gender=payload.gender
        clientCreate.age=payload.age

        return await this.clientRepository.save(clientCreate);
    }

    async Put(id:number,payload:UpdateClientDto){
        const clientFind:ClientEntity=await this.findOne(id);
        if(!clientFind){
            throw new  NotFoundException({
                message: "Cliente no encontrado",
                error: "no encontrado"
            })
        }

        const Updateclient:UpdateClientDto={...payload}

        /*clientCreate.identificationCard=payload.identificationCard
        clientCreate.names=payload.names
        clientCreate.email=payload.email
        clientCreate.phone=payload.phone
        clientCreate.gender=payload.gender
        clientCreate.age=payload.age
        clientCreate.creationDate=payload.creationDate*/

        return await this.clientRepository.update(id, Updateclient);
    }

    async changeState(id:number, payload:ChangeClientDto){
        const existingclient:ClientEntity = await this.clientRepository.findOne({
            where:{id:id}
        })

        if (!existingclient) {
            throw new NotFoundException({
                message: "Cliente no encontrado",
                error: "no encontrado"
            });
        }
    
        const updatedclient:ClientEntity = { ...existingclient, ...payload};
        
        return await this.clientRepository.update(id, updatedclient);
    }

    async delete(id:number){
        const clientFind= await this.findOne(id);

        if(!clientFind){
            throw new  NotFoundException({
                message: "Cliente no encontrado",
                error: "no encontrado"
            })
        }

        return await this.clientRepository.delete(id)
    }
}