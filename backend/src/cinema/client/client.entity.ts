
import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { clientPremiereEntity } from "../client_premiere/client_premiere.entity";

@Entity({name:"client"})
export class ClientEntity{
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({
        name:"indentification_card",
        type: "varchar",
        length: "10"
    })
    identificationCard: string;

    @Column({
        name:"names",
        type: "varchar",
        length: "80"
    })
    names: string;

    @Column({
        name:"email",
        type: "varchar",
        length: "50"
    })
    email: string;

    @Column({
        name:"phone",
        type: "varchar",
        length: "13"
    })
    phone: string;

    @Column({
        name:"gender",
        type: "varchar",
        length: "25"
    })
    gender: string;

    @Column({
        name:"age",
        type: "integer",
    })
    age: number;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date
    
    @OneToMany(() => clientPremiereEntity, clientpremiere => clientpremiere.idClient, { cascade: true })
    clientPremieres: clientPremiereEntity[]
}