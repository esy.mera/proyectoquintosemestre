
import { CreateRoleDto } from "./DTO/createrole.dto";
import { roleEntity } from "./role.entity";
import { ChangeRoleDto } from "./DTO/Change-state-role.dto";
import { UpdateRoleDto } from "./DTO/UpdateRole.dto";
import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Like, Repository } from "typeorm";
import { globalAplicationDTO } from "src/global-dto/global-aplication.dto";

@Injectable()
export class roleService {
    constructor(@InjectRepository(roleEntity) private roleRepository: Repository<roleEntity>){}

    async find({offset=1,limit=50, search}:globalAplicationDTO){
        const role: roleEntity[]= await this.roleRepository.find({
            relations:['employes'],
            skip: limit * (offset - 1),
            take: limit,
            where: search
                ? [
                    {
                        description: Like(`%${search}%`),
                    },
                ]
                : undefined,
        });
        return role;
    }

    async findOne(id:number){
        const roleFind:roleEntity=await this.roleRepository.findOne({
            where:{id:id},
            relations:['employes']
        })

        if(!roleFind){
            throw new  NotFoundException({
                message: "La premier del cliente no encontrado",
                error: "no encontrado"
            })
        }

        return roleFind;
    }

    async create(payload:CreateRoleDto){
        const roleCreate:roleEntity=this.roleRepository.create();
        roleCreate.description=payload.description
       

        return await this.roleRepository.save(roleCreate);
    }

    async Put(id:number,payload:UpdateRoleDto){
        const roleFind:roleEntity=await this.findOne(id);
        if(!roleFind){
            throw new  NotFoundException({
                message: "La premier del cliente no encontrado",
                error: "no encontrado"
            })
        }

        const Updaterole:UpdateRoleDto={...payload}

        /*client_premiereCreate.seat=payload.seat
        client_premiereCreate.creationDate=payload.creationDate*/

        return await this.roleRepository.update(id, Updaterole);
    }

    async changeState(id:number, payload:ChangeRoleDto){
        const existingrole:roleEntity = await this.roleRepository.findOne({
            where:{id:id}
        })

        if (!existingrole) {
            throw new NotFoundException({
                message: "La premier del cliente no encontrado",
                error: "no encontrado"
            });
        }
    
        const updatedrole:roleEntity = { ...existingrole, ...payload};
        
        return await this.roleRepository.update(id, updatedrole);
    }

    async delete(id:number){
        const roleFind= await this.findOne(id);

        if(!roleFind){
            throw new  NotFoundException({
                message: "La premier del cliente no encontrado",
                error: "no encontrado"
            })
        }

        return await this.roleRepository.delete(id)
    }
}