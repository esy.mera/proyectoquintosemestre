import { PartialType } from "@nestjs/swagger";
import { roleDto } from "./role.dto";

export class ChangeRoleDto extends PartialType (roleDto){
    
}