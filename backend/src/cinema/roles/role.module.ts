import { Module } from "@nestjs/common";
import { roleEntity } from "./role.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import { roleController } from "./role.controller";
import { roleService } from "./role.service";

@Module({
    imports: [TypeOrmModule.forFeature([roleEntity])],
    controllers:[roleController],
    providers:[roleService]
})
export class roleModule{}