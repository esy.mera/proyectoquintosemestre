import { Controller, Get, Param, Post, Body, Put, ParseIntPipe, Patch, Delete, Query } from "@nestjs/common";
import { ApiTags, ApiOperation } from "@nestjs/swagger";
import { ChangeRoleDto } from "./DTO/Change-state-role.dto";
import { UpdateRoleDto } from "./DTO/UpdateRole.dto";
import { CreateRoleDto } from "./DTO/createrole.dto";
import { roleService } from "./role.service";
import { globalAplicationDTO } from "src/global-dto/global-aplication.dto";

@ApiTags('role')
@Controller('role')
export class roleController {

    constructor(private readonly ClientPremiereService:roleService){}

    @ApiOperation({ description: "find role", summary: "find role", })
    @Get()
    async find(@Query() pagination:globalAplicationDTO) {
        return await this.ClientPremiereService.find(pagination)
    }

    @ApiOperation({ description: "find One role", summary: "find One role", })
    @Get(':id')
    async findOne(@Param('id') id: number) {
        return await this.ClientPremiereService.findOne(id);
    }


    @ApiOperation({ description: "Create role", summary: "Create role", })
    @Post()
    async create(@Body() payload: CreateRoleDto) {
        return await this.ClientPremiereService.create(payload);
    }



    @ApiOperation({ description: "update role", summary: "update role", })
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdateRoleDto) {
        return await this.ClientPremiereService.Put(id,payload);
    }


    @ApiOperation({ description: "change role", summary: "change role", })
    @Patch(':id')
    async changeState(@Param('id', ParseIntPipe) id: number, @Body() payload:ChangeRoleDto) {
        return await this.ClientPremiereService.changeState(id,payload);
    }


    @ApiOperation({ description: "delete role", summary: "delete role", })
    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number) {
        return await this.ClientPremiereService.delete(id);
    }
}