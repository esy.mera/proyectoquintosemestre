
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { EmployeeEntity } from "../employee/employee.entity";

@Entity({name:"role"})
export class roleEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column({
        name:"description",
        type: "varchar",
        length: "25"
    })
    description: string;

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date

    @OneToMany(() => EmployeeEntity, employe => employe.idRole, { cascade: true })
    employes: EmployeeEntity[]

}