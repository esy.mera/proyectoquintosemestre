import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { config_env } from './config/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle(config_env.SWAGGER.SWAGGER_TITLE)
    .setDescription(config_env.SWAGGER.SWAGGER_DESCRIPTION)
    .setVersion(config_env.SWAGGER.SWAGGER_VERSION)
    .addTag(config_env.SWAGGER.SWAGGER_TAG)
    .build();

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup(config_env.SWAGGER.SWAGGER_ROUTE, app, document);
  
  app.useGlobalPipes(new ValidationPipe(
    {
      errorHttpStatusCode:422,
      stopAtFirstError:true,
      whitelist: true, 
      forbidNonWhitelisted:true,
      transformOptions:{
        enableImplicitConversion:true
      }
    }
  ));

  app.enableCors();

  await app.listen(config_env.APP.APP_PORT);
}
bootstrap();
