import { modules_all } from './imports/modules';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SqlLoadService } from './sql-load/sql-load.service';


@Module({
  
  imports: [...modules_all],
  controllers: [AppController],
  providers: [AppService, SqlLoadService],
})
export class AppModule {}
