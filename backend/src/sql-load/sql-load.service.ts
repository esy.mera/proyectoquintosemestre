import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import * as fs from 'fs';
import { InjectConnection } from '@nestjs/typeorm';
import { config_env } from 'src/config/config';

@Injectable()
export class SqlLoadService {
    constructor(@InjectConnection() private readonly connection: Connection) {}

  async onModuleInit(): Promise<void> {
    if (config_env.APP.SQL_AUTOLOAD==true) {
      await this.loadSqlFile('src/sql-load/base.sql');
    } else {
      console.log('datos ya cargados')
    }
    
  }

  private async loadSqlFile(filePath: string): Promise<void> {
    try {
      const sqlContent = fs.readFileSync(filePath, 'utf-8');
      await this.connection.query(sqlContent);
      console.log('Archivo SQL cargado exitosamente.');
    } catch (error) {
      console.error('Error al cargar el archivo SQL:', error);
    }
  }
}
