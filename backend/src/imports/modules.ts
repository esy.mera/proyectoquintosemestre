import { BranchModule } from "src/cinema/branch/branch.module";
import { CategoryModule } from "src/cinema/category/category.module";
import { CityModule } from "src/cinema/city/city.module";
import { ClientModule } from "src/cinema/client/client.module";
import { clientPremiereModule } from "src/cinema/client_premiere/client_premiere.module";
import { EmployeeModule } from "src/cinema/employee/employee.module";
import { ImageModule } from "src/cinema/image/image.module";
import { LoginModule } from "src/cinema/login/login.module";
import { MovieModule } from "src/cinema/movie/movie.module";
import { PremiereModule } from "src/cinema/premiere/premiere.module";
import { RecoverPasswordModule } from "src/cinema/recover-password/recover-password.module";
import { roleModule } from "src/cinema/roles/role.module";
import { conexion_base_relacional } from "src/config/conexion-base-relacional";
import { global_config } from "src/config/config";


export const modules_all=[
    global_config,
    conexion_base_relacional,
    MovieModule, 
    PremiereModule, 
    clientPremiereModule,
    roleModule,
    ClientModule, 
    EmployeeModule, 
    ImageModule, 
    CategoryModule, 
    BranchModule, 
    CityModule,
    RecoverPasswordModule,
    LoginModule
]