import { ConfigModule } from "@nestjs/config";

export const global_config=ConfigModule.forRoot({
    isGlobal: true,
  })

export const config_env={
  APP:{
    APP_PORT: parseFloat(process.env.APP_PORT),
    SQL_AUTOLOAD: process.env.SQL_AUTOLOAD.toLowerCase() === 'true'
  },
  SWAGGER:{
    SWAGGER_TITLE: process.env.SWAGGER_TITLE,
    SWAGGER_DESCRIPTION: process.env.SWAGGER_DESCRIPTION,
    SWAGGER_VERSION: process.env.SWAGGER_VERSION,
    SWAGGER_TAG: process.env.SWAGGER_TAG,
    SWAGGER_ROUTE: process.env.SWAGGER_ROUTE
  },
  BDD:{
    BDD_HOST: process.env.BDD_HOST,
    BDD_PORT: parseFloat(process.env.BDD_PORT),
    BDD_USERNAME: process.env.BDD_USERNAME,
    BDD_PASSWORD: process.env.BDD_PASSWORD,
    BDD_DATABASE: process.env.BDD_DATABASE,
    BDD_SYNCHRONIZE: process.env.BDD_SYNCHRONIZE.toLowerCase() === 'true',
  },
  MAILER:{
    MAIL_HOST: process.env.MAIL_HOST,
    MAIL_PORT: parseFloat(process.env.MAIL_PORT),
    MAIL_SECURE: process.env.MAIL_SECURE.toLowerCase() === 'true',
    MAIL_USER: process.env.MAIL_USER,
    MAIL_PASSWORD: process.env.MAIL_PASSWORD
  }
}