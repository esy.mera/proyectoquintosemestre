import { TypeOrmModule } from "@nestjs/typeorm";
import { config_env } from "./config";


export const conexion_base_relacional=TypeOrmModule.forRoot({
    type: 'postgres',
    host: config_env.BDD.BDD_HOST,
    port: config_env.BDD.BDD_PORT,
    username: config_env.BDD.BDD_USERNAME,
    password: config_env.BDD.BDD_PASSWORD,
    database: config_env.BDD.BDD_DATABASE,
    entities: ['dist/**/*.entity{.ts,.js}'],
    synchronize: config_env.BDD.BDD_SYNCHRONIZE,
  })