import { MailerModule } from "@nestjs-modules/mailer";
import { config_env } from "./config";

export const transport = MailerModule.forRoot({
  transport: {
    host: config_env.MAILER.MAIL_HOST,
    port: config_env.MAILER.MAIL_PORT,
    secure: config_env.MAILER.MAIL_SECURE,
    auth: {
      user: config_env.MAILER.MAIL_USER,
      pass: config_env.MAILER.MAIL_PASSWORD
    }
  }
})