import { IsNumber, IsOptional, IsPositive, IsString } from "class-validator";


export class globalAplicationDTO {
   @IsNumber()
   @IsPositive()
   @IsOptional()
   limit:number;

   @IsNumber()
   @IsPositive()
   @IsOptional()
   offset:number;

   @IsString()
   @IsOptional()
   search:string;
}